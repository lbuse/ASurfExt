#include "Data.h"
#include "Sphere.h"
#include "Cylinder.h"

// Constructor from heritage
PtCloud :: PtCloud()
{
}

// Filling function
void PtCloud::addPoint(Vector3d v1, Vector3d v2){
    point_with_normal pwn;
    pwn.p=v1;
    pwn.n=v2;
    ptlist.push_back(pwn);

}


// Acces functions (members)
Vector3d PtCloud ::
    getPoint(int i) const {
        return ptlist.at(i-1).p;
}

Vector3d PtCloud ::
    getNormal(int i) const {
        return ptlist.at(i-1).n;
}

int PtCloud ::
    getSize() const {
    return ptlist.size();
}

PtCloud::point_with_normal PtCloud::back(){
    return ptlist.back();
}

bool PtCloud::empty(){
    return ptlist.empty();
}

void PtCloud::push_back(point_with_normal p){
    ptlist.push_back(p);
}

void PtCloud::pop_back(){
    ptlist.pop_back();
}


void PtCloud ::
    drawPtCloud() const {
    double pointRadius = 0.025;
    Vector3d zero3d(0,0,0);
    int nbpts=ptlist.size();
    for (int i=0; i<nbpts; i++) {
        Vector3d pt=ptlist.at(i).p;
        Sphere ptdraw(pt,pointRadius*2);
        glColor3f(0.0f,1.0f,0.0f);
        ptdraw.openglu(50);
        Vector3d n=ptlist.at(i).n;
        if (n != zero3d ) {
            Cylinder ndraw(pt,n,pointRadius,-pointRadius*8,pointRadius*8);
            glColor3f(0.0f,0.0f,1.0f);
            ndraw.openglu(50);
        }
    }
}



