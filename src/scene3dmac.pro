# The simplest application example: 20 lines of code and yet all the power !

# A Viewer class is derived from QGLViewer and its <code>draw()</code> function is overloaded to
# specify the user defined OpenGL orders that describe the scene.

# This is the first example you should try, as it explains some of the default keyboard shortcuts
# and the mouse behavior of the viewer.

# This example can be cut and pasted to start the development of a new application.

TEMPLATE = app
TARGET   = scene3d

HEADERS  = Data.h \
           Plane.h \
           Sphere.h \
           Viewer.h \
           Cylinder.h \
           Cylinders.h \
           Cone.h \
           Cones.h \
           Shape.h \
           Ransac.h
		   	
SOURCES  = main.cpp \
           Data.cpp \
           Viewer.cpp \
           Plane.cpp \
           Sphere.cpp \
           Cylinder.cpp \
           Cylinders.cpp \
           Cone.cpp \
           Cones.cpp \
           Shape.cpp \
           Ransac.cpp


QT *= xml opengl widgets gui

CONFIG += qt opengl warn_on thread rtti console embed_manifest_exe no_keywords

# --------------------------------------------------------------------------------------

# The remaining of this configuration tries to automatically detect the library paths.
# In your applications, you can probably simply use (see doc/compilation.html for details) :

# Change these paths according to your configuration.

INCLUDEPATH += /Users/lbuse/DevCode/eigen-eigen-c58038c56923/
INCLUDEPATH += /opt/local/include/lapack

# --------------------------------------------------------------------------------------

CONFIG *= release

# Set path to include and lib files (see doc/compilation.html for details):
# Uncomment and tune these paths according to your configuration.

INCLUDEPATH *= /Users/lbuse/DevCode/libQGLViewer-2.6.3

!plugin:QMAKE_POST_LINK=install_name_tool -change QGLViewer.framework/Versions/2/QGLViewer /Users/lbuse/DevCode/libQGLViewer-2.6.3/QGLViewer/QGLViewer.framework/Versions/2/QGLViewer $${TARGET}.app/Contents/MacOS/$${TARGET} #VERSION_MAJOR
LIBS += -F/Users/lbuse/DevCode/libQGLViewer-2.6.3/QGLViewer -framework QGLViewer
LIBS += -llapack
ICON = /Users/lbuse/DevCode/libQGLViewer-2.6.3/QGLViewer/qglviewer.icns

# --------------------------------------------------------------------------------------

