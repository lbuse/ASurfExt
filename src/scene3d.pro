# The simplest application example: 20 lines of code and yet all the power !

# A Viewer class is derived from QGLViewer and its <code>draw()</code> function is overloaded to
# specify the user defined OpenGL orders that describe the scene.

# This is the first example you should try, as it explains some of the default keyboard shortcuts
# and the mouse behavior of the viewer.

# This example can be cut and pasted to start the development of a new application.

TEMPLATE = app
TARGET   = scene3d

HEADERS  = Data.h \
           Plane.h \
           Sphere.h \
           Viewer.h \
           Cylinder.h\
           Cylinders.h\
           Cone.h \
           Cones.h \
           Torus.h\
           Tori.h
		   	
SOURCES  = main.cpp \
           Data.cpp \
           Viewer.cpp \
           Plane.cpp \
           Sphere.cpp \
           Cylinder.cpp\
           Cylinders.cpp \
           Cone.cpp \
           Torus.cpp\
           Tori.cpp \
           Cones.cpp



QT *= xml opengl widgets gui

CONFIG += qt opengl warn_on thread rtti console embed_manifest_exe no_keywords std=c++11

# --------------------------------------------------------------------------------------

# The remaining of this configuration tries to automatically detect the library paths.
# In your applications, you can probably simply use (see doc/compilation.html for details) :

# Change these paths according to your configuration.

#INCLUDEPATH += /usr/include/CGAL
INCLUDEPATH += /usr/include/GL
INCLUDEPATH += /usr/include/R
#INCLUDEPATH += /usr/include
#INCLUDEPATH += /usr/include/bits
INCLUDEPATH += /usr/include/eigen3
#INCLUDEPATH += /usr/include/eigen3







# --------------------------------------------------------------------------------------

#CONFIG *= release

# Set path to include and lib files (see doc/compilation.html for details):
# Uncomment and tune these paths according to your configuration.

#INCLUDEPATH += /home/adrien/QGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4



#LIBS += "-L/usr/lib64 -lGL -lm -llapack -lpthread -lX11 -lz libGLU.so.1.3.1"

LIBS += -L/usr/lib64 -llapack -lm -lpthread -lGL -lGLU

#LIBS += -L/usr/lib64 -lpthread
#LIBS += -L/usr/lib64 -lGL
#LIBS += -L/usr/lib64 -lGLU

#!plugin:QMAKE_POST_LINK=install_name_tool -change QGLViewer.framework/Versions/2/QGLViewer /user/adeharve/home/librairies/libQGLViewer-2.6.3/QGLViewer/QGLViewer.framework/Versions/2/QGLViewer $${TARGET}.app/Contents/MacOS/$${TARGET} #VERSION_MAJOR
#LIBS += -L/home/adrien/QGL/libQGLViewer-2.6.4/QGLViewer -lQGLViewer-qt5

#LIBS += "-L /usr/lib64 -lGL -lGLU"
#LIBS += "-L/usr/lib64 -lGL"
#LIBS += -L/usr/lib64 -llapack

#ICON = /home/adrien/Documents/QGL/libQGLViewer-2.7.0/libQGLViewer-2.7.0/QGLViewer/qglviewer.icns

# --------------------------------------------------------------------------------------





win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../Documents/libQGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4/QGLViewer/release/ -lQGLViewer-qt5
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../Documents/libQGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4/QGLViewer/debug/ -lQGLViewer-qt5
else:unix: LIBS += -L$$PWD/../../../../../../Documents/libQGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4/QGLViewer/ -lQGLViewer-qt5

INCLUDEPATH += $$PWD/../../../../../../Documents/libQGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4/QGLViewer
DEPENDPATH += $$PWD/../../../../../../Documents/libQGL/libQGLViewer-2.6.4/libQGLViewer-2.6.4/QGLViewer
