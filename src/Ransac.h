/* **************** *
 * not functionning *
 * **************** */





#ifndef RANSAC_H
#define RANSAC_H

#include <Eigen/Dense>
//#include <QGLViewer/qglviewer.h>
#include <vector>
#include <Shape.h>
#include <Data.h>

using namespace Eigen;

class Ransac
{
public:
// shape with points : une shape et les points associes
struct shape_wp{
    //Shape *s;
    PtCloud p;
};
//**Constructors
Ransac(PtCloud);
//**Members
void addShape();
void displayInfo();
void openglu(int);
void testData();
int random(int i);

private:
PtCloud data;
std::vector<shape_wp> solution;


};

#endif // RANSAC_H
