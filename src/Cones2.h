#ifndef CONES2_H
#define CONES2_H

#include <Eigen/Dense>
#include <vector>
#include <Cone.h>
#include <Shape.h>
#include "gl.h"
#include "glu.h"
#include <iostream>
using namespace Eigen;
using namespace std;

class Cones2// : public virtual Shape
{

private:
    //**Attributes
    std::vector<Cone> list;                     //* List of cones

public:
    //**Constructors
    Cones2();
    Cones2(const Vector3d&,const Vector3d&,const Vector3d&,
          const Vector3d&);                                     //* 2 points and their normal vectors define 0 or 2 Cones
    Cones2(const Vector3d&,const Vector3d&,const Vector3d&,
          const Vector3d&,const Vector3d&);                     //* 1 points with normal and 3 points define 0, 2 or 4 Cones
    Cones2(const Vector3d&,const Vector3d&,const Vector3d&,
          const Vector3d&,const Vector3d&,const Vector3d&);     //* 6 points define up to 6 Cones

    //**Members
    Cone get(int);                              //* Get the cone in position i in the list
    unsigned int size();                        //* Get the amount of Cones in the list
    void displayInfo();                         //* Displays the informations about all the Cones in the list
    void openglu(int);                          //* Draws all the Cones inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    double power(double,int);                   //* Returns double^int
    double dist(Vector3d,int);                  //* Absolute distance between the figure in argument (int) and a point in argument (Vector3d)
};

#endif
