#include "Cylinder.h"
#define THRESHOLD 0.0001
Cylinder::
    Cylinder() {
    }


Cylinder::
    Cylinder(const Vector3d& center, const Vector3d& axis,   double radius, double l1, double l2) {

    // Test if the Normal is not 0 0 0
    if ((abs(axis(0))<THRESHOLD) && (abs(axis(1))<THRESHOLD) && (abs(axis(2))<THRESHOLD)){
        std::cerr<< "axis is undefined"<< endl;
        return;
    }
    if(abs(radius) < THRESHOLD){
        std::cerr<< "radius is lower than 0.0001"<< endl;
    }
        this->center = center;
        this->axis = axis;
        this->radius = radius;
        this->length_max=max(l1,l2);
        this->length_min=min(l1,l2);
    }

Cylinder::
    Cylinder(const Vector3d& v1, const Vector3d& v2, const Vector3d& v3) {
    // Test if 2 points are equals
    if (((v1-v2).norm() < THRESHOLD) || ((v1-v3).norm() < THRESHOLD) || ((v2-v3).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
        double alpha  = (v2-v3).squaredNorm()*(v1-v2).dot(v1-v3)/(2*(v1-v2).cross(v2-v3).squaredNorm());
        double beta   = (v1-v3).squaredNorm()*(v2-v1).dot(v2-v3)/(2*(v1-v2).cross(v2-v3).squaredNorm());
        double gamma  = (v1-v2).squaredNorm()*(v3-v1).dot(v3-v2)/(2*(v1-v2).cross(v2-v3).squaredNorm());
        this-> radius = (v1-v2).norm()*(v2-v3).norm()*(v3-v1).norm()/(2*((v1-v2).cross(v2-v3)).norm());
        this-> center = alpha*v1 + beta*v2 + gamma*v3;
        this-> axis   = (v2-v1).cross(v3-v1)/((v2-v1).cross(v3-v1).norm());
    }

void Cylinder::
    distance( Vector3d vector) {
    double distance = abs(norm(crossProduct((vector-(this->center)),(this->axis)))/norm(this->axis) - (this->radius));
    if (distance <= THRESHOLD) {
        cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to inlier !"<<endl;
    } else {
        cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to outlier !"<<endl;
    }
}

void Cylinder::
    displayInfo() {
        std::cout<<"The circumcenter of current cylinder is:\n"<<this->center<<std::endl;
        std::cout<<"The axis direction is:\n"<<this->axis.normalized()<<std::endl;
        std::cout<<"The radius is:\n"<<this->radius<<std::endl;
    }

void Cylinder::
    openglu(int precision) {
        GLUquadric *quad;
        quad = gluNewQuadric();
        glMatrixMode(GL_MODELVIEW);
        gluQuadricDrawStyle(quad,GLU_LINE);
        glPushMatrix();
        Vector3d n=this->axis.normalized();
        Vector3d v;
        if ((n(0)!=0) | (n(1)!=0)){
            v=Vector3d(n(1),-n(0),0);
        }
        else{
            v=Vector3d(0,-n(2),n(1));
        }
        v=v.normalized();
        Vector3d b(v(1)*n(2)-v(2)*n(1),v(2)*n(0)-v(0)*n(2),v(0)*n(1)-v(1)*n(0));
        b=b.normalized();
        GLdouble Rotation[16]={b(0),    v(0),   n(0),   this->center[0]+n(0)*length_min,
                               b(1),    v(1),   n(1),   this->center[1]+n(1)*length_min,
                               b(2),    v(2),   n(2),   this->center[2]+n(2)*length_min,
                               0,       0,      0,      1};
        glMultTransposeMatrixd(Rotation);
        gluCylinder(quad,this->radius,this->radius,(length_max-length_min),precision,precision);
        glPopMatrix();
        gluDeleteQuadric(quad);
    }

double Cylinder::
    dotProduct(Vector3d v1, Vector3d v2){
    return v1(0)*v2(0)+v1(1)*v2(1)+v1(2)*v2(2);
}

Vector3d Cylinder::
    crossProduct(Vector3d v1, Vector3d v2){
    return Vector3d(v1(1)*v2(2)-v1(2)*v2(1),
                    v1(2)*v2(0)-v1(0)*v2(2),
                    v1(0)*v2(1)-v1(1)*v2(0));
}

double Cylinder::
    norm(Vector3d v){
    return sqrt(dotProduct(v,v));
}

double Cylinder::
    dist(Vector3d p){
    return fabs(norm(crossProduct(p-center,axis))/norm(axis)-radius);
}
