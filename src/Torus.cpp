#include "Torus.h"

Torus::
    Torus() {
    }

Torus::
    Torus(const Vector3d& center, const Vector3d& axis, double large_radius, double small_radius, double angle_min, double angle_max) {
        // Test if the Normal is not 0 0 0
        if ((abs(axis(0))<THRESHOLD) && (abs(axis(1))<THRESHOLD) && (abs(axis(2))<THRESHOLD)){
            std::cerr<< "axis is undefined"<< endl;
            return;
        }
        // Torus is degenerated
        if((large_radius-small_radius) < THRESHOLD){
            std::cerr<< "large radius is lower than the small radius"<< endl;
            return;
        }

        this->center = center;
        this->axis = axis;
        this->large_radius = large_radius;
        this->small_radius = small_radius;
        this->angle_start=angle_min;
        this->angle_end=angle_max;
    }

double Torus::
    dist(Vector3d vector){
    Vector3d p = center+ dotProduct(vector-center,axis)/dotProduct(axis,axis)*axis ;
    Vector3d c1 = center+ large_radius*(vector-p)/norm(vector-p);
    return norm(vector-c1)-small_radius;
}

double Torus::
    normal_deviation(Vector3d point3, Vector3d normal3){
    double teta=0; // radian angle between the computed and real normal
    Vector3d p = center+ dotProduct(point3-center,axis)/dotProduct(axis,axis)*axis ;
    Vector3d c1 = center+ large_radius*(point3-p)/norm(point3-p);
    Vector3d dir=(p-c1).normalized();

    teta=std::acos(dotProduct(normal3,dir)/(norm(normal3)*norm(dir)));
    if (abs (teta-M_PI)<0.000001){ // teta and angle: supplementary angles
        teta=0;

    }
    std::cout<<"teta:"<<teta<<endl;
    return teta;

}






void Torus::
    distance( Vector3d vector) {
    double distance = dist(vector);
    if (distance <= THRESHOLD) {
        std::cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to inlier !"<<std::endl;
    } else {
        std::cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to outlier !"<<std::endl;
    }
}

void Torus::
    displayInfo() {
        std::cout<<"The center of current Torus is:\n"<<this->center<<std::endl;
        std::cout<<"The axis direction is:\n"<<this->axis.normalized()<<std::endl;
        std::cout<<"The large radius is:\n"<<this->large_radius<<std::endl;
        std::cout<<"The small radius is:\n"<<this->small_radius<<std::endl;
    }

void Torus::
    openglu(int precision) {
        for(int i = 0; i< precision; i++){
            double angle = 2*my_pi*i/precision;
            GLUquadric *quad;
            quad = gluNewQuadric();
            glMatrixMode(GL_MODELVIEW);
            gluQuadricDrawStyle(quad,GLU_LINE);
            glPushMatrix();
            Vector3d n=this->axis.normalized();
            Vector3d v;
            if ((n(0)!=0) | (n(1)!=0)){
                v=Vector3d(n(1),-n(0),0);
            }
            else{
                v=Vector3d(0,-n(2),n(1));
            }
            v=v.normalized();
            Vector3d b=crossProduct(v,n);
            b=b.normalized();
            Vector3d p = v*cos(angle)+b*sin(angle);
            p = p.normalized();
            Vector3d q = crossProduct(n,p); // axis of the cylinder which will compose the torus
            q = q.normalized();
            GLdouble Rotation[16]={n(0),    p(0),   q(0),   this->center(0)+p(0)*large_radius,
                                   n(1),    p(1),   q(1),   this->center(1)+p(1)*large_radius,
                                   n(2),    p(2),   q(2),   this->center(2)+p(2)*large_radius,
                                   0,       0,      0,      1};

            glMultTransposeMatrixd(Rotation);
            gluCylinder(quad,small_radius,small_radius,large_radius*4*my_pi/precision,precision,2);
            glPopMatrix();
            gluDeleteQuadric(quad);
        }
    }

double Torus::
    dotProduct(Vector3d v1, Vector3d v2){
    return v1(0)*v2(0)+v1(1)*v2(1)+v1(2)*v2(2);
}

Vector3d Torus::
    crossProduct(Vector3d v1, Vector3d v2){
    return Vector3d(v1(1)*v2(2)-v1(2)*v2(1),
                    v1(2)*v2(0)-v1(0)*v2(2),
                    v1(0)*v2(1)-v1(1)*v2(0));
}

double Torus::
    norm(Vector3d v){
    return sqrt(dotProduct(v,v));
}


