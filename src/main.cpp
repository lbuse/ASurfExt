#include "stdlib.h"
#include "Plane.h"
#include "Sphere.h"
#include "Viewer.h"
#include <qapplication.h>
#include "Cylinder.h"
#include "Cylinders.h"
#include "Cone.h"
#include "Cones.h"
#include "Torus.h"
#include "Tori.h"

#include <Eigen/Dense>
#include <math.h>
#include "R_ext/Lapack.h"
#include "stdio.h"      /* printf, scanf, puts, NULL */ /* srand, rand */
#include <fstream>
#include <iostream>
#include <cstdio>
#include <ctime>


using namespace std;
using namespace Eigen;


double my_random()
{
    double d= (double)((int)rand() % 1000)/1000;
    return d;
}

double my_random(int min,int max){
    return my_random()*(max-min)+min;
}







int main(int argc, char** argv) {

    /* Give a number N of tests to run
 *
 * If N = 0, the program jumps to the Viewer,and you can edit Viewer.cpp
 * to choose what kind of figure you want to generate.
 *
 * If N > 0, the program generates random points, and launches a simulation
 * over N shapes of each type, and prints the repartition of said figure,
 * plus information about the calculation time.
 * *    int N=00000;

 */



int N=000;

if(N>0){
//        srand(time(NULL));
//        //* Generating points
//        PtCloud PtMat;
//        for(int i=0;i<6*N;i++){
//            PtMat.addPoint(Vector3d(2*my_random()-1,    2*my_random()-1,    2*my_random()-1),Vector3d(my_random()-0.5,  my_random()-0.5,  my_random()-0.5));
//        }

//        //*************************************************************************
//        std::cout<<"\n\nSphere, 4 points\n";
////        auto t_sphere4_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Sphere sp(PtMat.getPoint(4*i+1),PtMat.getPoint(4*i+2),PtMat.getPoint(4*i+3),PtMat.getPoint(4*i+4));
//        }
////        auto t_sphere4_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_sphere4_2 - t_sphere4_1).count()/N
//                  << " nanoseconds" << std::endl;

//        //*************************************************************************
//        std::cout<<"\n\nSphere, 2 points with 1 normal\n";
////        auto t_sphere2_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Sphere sp(PtMat.getPoint(2*i+1),PtMat.getNormal(2*i+1),PtMat.getPoint(2*i+2));
//        }
////        auto t_sphere2_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_sphere2_2 - t_sphere2_1).count()/N
//                  << " nanoseconds" << std::endl;

//        //*************************************************************************
//        std::cout<<"\n\nPlane, 3 points\n";
////        auto t_plane3_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Plane pl(PtMat.getPoint(3*i+1),PtMat.getPoint(3*i+2),PtMat.getPoint(3*i+3));
//        }
////        auto t_plane3_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_plane3_2 - t_plane3_1).count()/N
//                  << " nanoseconds" << std::endl;

//        //*************************************************************************
//        std::cout<<"\n\nPlane, 1 point with normal\n";
////        auto t_plane1_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Plane pl(PtMat.getPoint(i+1),PtMat.getNormal(i+1));
//        }
////        auto t_plane1_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_plane1_2 - t_plane1_1).count()/N
//                  << " nanoseconds" << std::endl;

        //*************************************************************************
//        std::cout<<"\n\nCones, 2 oriented points\n";
//        double num_cone2_two=0;
//        double num_cone2_zero=0;
////        auto t_cone2_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Cones cy(PtMat.getPoint(2*i+1),PtMat.getNormal(2*i+1),PtMat.getPoint(2*i+2),PtMat.getNormal(2*i+2));
//            if(cy.size()>0){
//                num_cone2_two++;
//            }
//            else{
//                num_cone2_zero++;
//            }
//        }
////        auto t_cone2_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_cone2_2 - t_cone2_1).count()/N
//                  << " nanoseconds" << std::endl;
//        std::cout << "2 cones detected: "<<num_cone2_two
//                  << "\n0 cones detected: "<<num_cone2_zero
//                  << "\nPercentage of detections : " <<num_cone2_two/(num_cone2_two+num_cone2_zero)*100<<"%"<< std::endl;

        //*************************************************************************
//        std::cout<<"\n\nCones, 1 oriented point + 3 points\n";
//        double num_cone4_four=0;
//        double num_cone4_two=0;
//        double num_cone4_zero=0;
////        auto t_cone4_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Cones cy(PtMat.getPoint(3*i+1),PtMat.getNormal(3*i+1),PtMat.getPoint(3*i+2),PtMat.getPoint(3*i+3),PtMat.getPoint(3*i+4));
//            if(cy.size()>2){
//                num_cone4_four++;
//            }
//            else if(cy.size()>0){
//                num_cone4_two++;
//            }
//            else{
//                num_cone4_zero++;
//            }
//        }
////        auto t_cone4_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_cone4_2 - t_cone4_1).count()/N
//                  << " nanoseconds" << std::endl;
//        std::cout << "4 cones detected: "<<num_cone4_four
//                  << "\n2 cones detected: "<<num_cone4_two
//                  << "\n0 cones detected: "<<num_cone4_zero
//                  << "\nPercentage of detections (4 cones): " <<num_cone4_four/(num_cone4_two+num_cone4_zero+num_cone4_four)*100<<"%"
//                  << "\nPercentage of detections (2 cones): " <<num_cone4_two/(num_cone4_two+num_cone4_zero+num_cone4_four)*100<<"%"
//                  << "\nPercentage of detections (0 cones): " <<num_cone4_zero/(num_cone4_two+num_cone4_zero+num_cone4_four)*100<<"%"<< std::endl;

        //*************************************************************************
//        std::cout<<"\n\nCones, 6 points\n";
//        double num_cone12_six=0;
//        double num_cone12_four=0;
//        double num_cone12_two=0;
//        double num_cone12_zero=0;
////        auto t_cone12_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Cones cy(PtMat.getPoint(6*i+1),PtMat.getPoint(6*i+2),PtMat.getPoint(6*i+3),PtMat.getPoint(6*i+4),PtMat.getPoint(6*i+5),PtMat.getPoint(6*i+6));
//            if(cy.size()>5){
//                num_cone12_six++;
//            }
//            else if(cy.size()>3){
//                num_cone12_four++;
//            }
//            else if(cy.size()>1){
//                num_cone12_two++;
//            }
//            else{
//                num_cone12_zero++;
//            }
//        }
////        auto t_cone12_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_cone12_2 - t_cone12_1).count()/N
//                  << " nanoseconds" << std::endl;
//        std::cout << "6 cones detected: "<<num_cone12_six
//                  << "\n4 cones detected: "<<num_cone12_four
//                  << "\n2 cones detected: "<<num_cone12_two
//                  << "\n0 cones detected: "<<num_cone12_zero
//                  << "\nPercentage of detections (6 cones): " <<num_cone12_six/(num_cone12_six+num_cone12_four+num_cone12_two+num_cone12_zero)*100<<"%"
//                  << "\nPercentage of detections (4 cones): " <<num_cone12_four/(num_cone12_six+num_cone12_four+num_cone12_two+num_cone12_zero)*100<<"%"
//                  << "\nPercentage of detections (2 cones): " <<num_cone12_two/(num_cone12_six+num_cone12_four+num_cone12_two+num_cone12_zero)*100<<"%"
//                  << "\nPercentage of detections (0 cones): " <<num_cone12_zero/(num_cone12_six+num_cone12_four+num_cone12_two+num_cone12_zero)*100<<"%"<< std::endl;

//        std::cout<<"\n\nCylinders, 1 oriented point + 2 points\n";
//        double num_cylinder3_two=0;
//        double num_cylinder3_zero=0;
////        auto t_cylinder3_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Cylinders cy(PtMat.getPoint(3*i+1),PtMat.getNormal(3*i+1),PtMat.getPoint(3*i+2),PtMat.getPoint(3*i+3));
//            if(cy.size()>0){
//                num_cylinder3_two++;
//            }
//            else{
//                num_cylinder3_zero++;
//            }
//        }
////        auto t_cylinder3_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_cylinder3_2 - t_cylinder3_1).count()/N
//                  << " nanoseconds" << std::endl;
//        std::cout << "2 cylinders detected: "<<num_cylinder3_two
//                  << "\n0 cylinders detected: "<<num_cylinder3_zero
//                  << "\nPercentage: " <<num_cylinder3_two/(num_cylinder3_two+num_cylinder3_zero)*100<<"%"<< std::endl;

        //*************************************************************************
//        std::cout<<"\n\nCylinders, 5 points\n";
//        double num_cylinder5_two=0;
//        double num_cylinder5_four=0;
//        double num_cylinder5_six=0;
//        double num_cylinder5_zero=0;
////        auto t_cylinder5_1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Cylinders cy(PtMat.getPoint(5*i+1),PtMat.getPoint(5*i+2),PtMat.getPoint(5*i+3),PtMat.getPoint(5*i+4),PtMat.getPoint(5*i+5));
//            if(cy.size()>4){
//                num_cylinder5_six++;
//            }
//            else if(cy.size()>2){
//                num_cylinder5_four++;
//            }
//            else if(cy.size()>0){
//                num_cylinder5_two++;
//            }
//            else{
//                num_cylinder5_zero++;
//            }
//        }
////        auto t_cylinder5_2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_cylinder5_2 - t_cylinder5_1).count()/N
//                  << " nanoseconds" << std::endl;
//        std::cout << "6 cylinders detected: "<<num_cylinder5_six
//                  << "\n4 cylinders detected: "<<num_cylinder5_four
//                  << "\n2 cylinders detected: "<<num_cylinder5_two
//                  << "\n0 cylinders detected: "<<num_cylinder5_zero
//                  << "\nPercentage of detections (6 cylinders): " <<num_cylinder5_six/(num_cylinder5_two+num_cylinder5_zero+num_cylinder5_four+num_cylinder5_six)*100<<"%"
//                  << "\nPercentage of detections (4 cylinders): " <<num_cylinder5_four/(num_cylinder5_two+num_cylinder5_zero+num_cylinder5_four+num_cylinder5_six)*100<<"%"
//                  << "\nPercentage of detections (2 cylinders): " <<num_cylinder5_two/(num_cylinder5_two+num_cylinder5_zero+num_cylinder5_four+num_cylinder5_six)*100<<"%"
//                  << "\nPercentage of detections (0 cylinders): " <<num_cylinder5_zero/(num_cylinder5_two+num_cylinder5_zero+num_cylinder5_four+num_cylinder5_six)*100<<"%"<< std::endl;




        //*************************************************************************
//        std::cout<<"\n\nTori\n";
//        double tori[13];
//        for(int i=0; i<13; i++){
//            tori[i]=0;
//        }
//        int toto=0;
////        auto t_tori1 = Clock::now();
//        for(int i=0; i<N;i++){
//            Tori to(PtMat.getPoint(3*i+1),PtMat.getNormal(3*i+1),PtMat.getPoint(3*i+2),PtMat.getNormal(3*i+2),PtMat.getPoint(3*i+3));
//            tori[to.size()]++;
//            toto++;
//        }
////        auto t_tori2 = Clock::now();
//        std::cout << "Average time: "
////                  << std::chrono::duration_cast<std::chrono::nanoseconds>(t_tori2 - t_tori1).count()/N
//                  << " nanoseconds" << std::endl;
//        for(int i=0; i<13; i++){
//            std::cout << i <<" tori detected: "<<tori[i]
//                         << "\nPercentage of detections ("<<i<<" tori): " <<tori[i]/toto*100<<"%"<< std::endl;
//        }

    /* New timer because the "Clock::now() does'nt work*/

//        std::clock_t start;
//        std::clock_t stop;
//        start = std::clock();


//        for(int i=0;i<N;i++){
//            double c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15;  // random coordinates
//            ifstream fichier("test.txt", ios::in);                                              // opening of the file
//            if(fichier)
//            {

//                    fichier>>c1>>c2>>c3>>c4>>c5>>c6>>c7>>c8>>c9>>c10>>c11>>c12>>c13>>c14>>c15;
//                    fichier.close();

//            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }
//            std::string Buffer2 = "";
//            std::ifstream ReadFile2( "test.txt");
//            if (ReadFile2)
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile2, line ) )
//                {
//                    Line++;
//                    if(Line != 1)
//                        Buffer2 += line + "\n";
//                }
//            }
//            ReadFile2.close();

//            std::ofstream WriteFile2( "test.txt" );
//            WriteFile2 << Buffer2;
//            WriteFile2.close();
//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d n1(c7,c8,c9);
//            Vector3d n2(c10,c11,c12);
//            Vector3d p3(c13,c14,c15);
//            Tori t1 (p1,n1,p2,n2,p3);
//            std::ofstream ofs;
//            ofs.open ("nbtori.txt", std::ofstream::out | std::ofstream::app);
//            ofs<<t1.size()<<endl;
//            ofs.close();
//        }
//        stop = std::clock();
//        std::cout << "Average time: "<< ( (stop- start ) / (double) CLOCKS_PER_SEC)/N<< " seconds" << std::endl;













}
else{

        /* Drawing*/




        // Read command lines arguments.
        QApplication application(argc,argv);
        // Instantiate the viewer.
        Viewer viewer;



        viewer.setWindowTitle("scene3d");

        //viewer.setBackgroundColor(glColor3f(0.0f,0.0f,0.0f)); <- BackGround White
        // Make the viewer window visible on screen.


        viewer.show();

        // Run main loop.
        return application.exec();

//        viewer.draw();
//        return 1;


    }
};

