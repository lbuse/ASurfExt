﻿/****************************************************************************

 Copyright (C) 2002-2014 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.6.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License 
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain 
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

 Modified by Antoine DEHARVENG - 2016

*****************************************************************************/

#include "Viewer.h"
#include "Plane.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Cylinders.h"
#include "Cone.h"
#include "Cones.h"
#include "Data.h"
#include <Eigen/Dense>
#include "math.h"
#include "stdio.h"      /* printf, scanf, puts, NULL */
#include "Sphere.h"
#include "Tori.h"

#include <iostream>
#include <fstream>
using namespace std;
using namespace Eigen;




void Viewer::draw() {

//            QColor q(255,255,255);

//            setBackgroundColor(q);



            /* Reading of coordinates in a file "test.txt" in the same folder as the Build_Project*/
            double c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26;  // random coordinates
            ifstream fichier("test.txt", ios::in);                                              // opening of the file
            if(fichier)
            {

                    fichier>>c1>>c2>>c3>>c4>>c5>>c6>>c7>>c8>>c9>>c10>>c11>>c12>>c13>>c14>>c15>>c16>>c17>>c18>>c19>>c20>>c21>>c22>>c23>>c24>>c25>>c26;
                    fichier.close();

            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }
//            std::string Buffer2 = "";
//            std::ifstream ReadFile2( "test.txt");
//            if (ReadFile2)
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile2, line ) )
//                {
//                    Line++;
//                    if(Line != 1)
//                        Buffer2 += line + "\n";
//                }
//            }
//            ReadFile2.close();

//            std::ofstream WriteFile2( "test.txt" );
//            WriteFile2 << Buffer2;
//            WriteFile2.close();

            /* Choice of the object to view
             * comment the others
             */


            /* cone 6P */

//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d p3(c7,c8,c9);
//            Vector3d p4 (c10,c11,c12);
//            Vector3d p5 (c13,c14,c15);
//            Vector3d p6 (c16,c17,c18);
//            Cones C6(p1,p2,p3,p4,p5,p6);
//            C6.displayInfo();
//            C6.openglu(50);
//            Sphere S1(p1,0.1);
//            S1.openglu();
//            Sphere S2(p2,0.1);
//            S2.openglu();
//            Sphere S3(p3,0.1);
//            S3.openglu();
//            Sphere S4(p4,0.1);
//            S4.openglu();
//            Sphere S5(p5,0.1);
//            S5.openglu();
//            Sphere S6(p6,0.1);
//            S6.openglu();



            /* Torus constructors */

            Vector3d p1(c1,c2,c3);
            Vector3d p2(c4,c5,c6);
            Vector3d n1(c7,c8,c9);
            Vector3d n2(c10,c11,c12);
            Vector3d p3(c13,c14,c15);
            Vector3d n3(c16,c17,c18);
            Tori t1 (p1,n1,p2,n2,p3,n3);
            t1.get(0).openglu();
            t1.get(0).displayInfo();
            std::cout<<"------"<<endl;
            std::cout<<n3.normalized()<<endl;
            std::cout<<t1.get(0).normal_deviation(p3,n3)<<endl;

            Vector3d cy(0.191,-0.944,-0.268);
            PtCloud pc4;
            pc4.addPoint(p3,cy);
            pc4.drawPtCloud();
//            std::ofstream ofs;
//            if(t1.size()==8){
//                ofs.open ("nbtori.txt", std::ofstream::out | std::ofstream::app);
//                ofs <<"coordonne pour 8 tores"<<endl;
//                ofs << p1<<endl;
//                ofs << p2<<endl;
//                ofs << n1<<endl;
//                ofs << n2<<endl;
//                ofs << p3<<endl;
//                ofs<<t1.size()<<endl;
//                ofs.close();
//            }
//            std::cout<<"NB "<<t1.size()<<endl;


            PtCloud pc1;
            pc1.addPoint(p1,n1);
            pc1.drawPtCloud();
            PtCloud pc2;
            pc2.addPoint(p2,n2);
            pc2.drawPtCloud();
            PtCloud pc3;
            pc3.addPoint(p3,n3);
            pc3.drawPtCloud();



//            Vector3d center(c19,c20,c21);
//            Vector3d axis(c22,c23,c24);
//            Torus t(center,axis,c25,c26);
//            t.displayInfo();
//            t.openglu(50);

            Sphere C3(p3,0.1);
            C3.openglu();
            Vector3d o(0.19102,-0.944156,-0.268478);
            Sphere C22(o,0.1);
            C22.openglu();
//            Sphere C1(p1,0.1);
//            C1.openglu();
//            PtCloud dir;
//            dir.addPoint(t1.get(0).center,t1.get(0).axis);
//            dir.drawPtCloud();
//            t1.displayInfo();
//            t1.get(0).distance(p1);
//            t1.get(1).distance(p1);
//            t1.get(2).distance(p1);
//            t1.get(0).distance(p2);
//            t1.get(1).distance(p2);
//            t1.get(2).distance(p2);
//            t1.get(0).distance(p3);
//            t1.get(1).distance(p3);
//            t1.get(2).distance(p3);



//            Vector3d center(0,1,0);
//            Vector3d axis(0,1,0);
//            Torus t(center,axis,1.5,0.5);
//            t.openglu(80);
//            t.displayInfo();
//            PtCloud pc1;
//            pc1.addPoint(center,axis);
//            pc1.drawPtCloud();










//            Vector3d p1(c1,c2,c3);
//            Vector3d n1(c4,c5,c6);
//            Torus t1 (p1,n1,1,0.25);
//            t1.openglu();

//            PtCloud pc1;
//            pc1.addPoint(p1,n1);
//            pc1.drawPtCloud();



            //Sphere C1(p1,0.01);
            //Sphere C2(p2,0.01);
            //C1.openglu();
            //C2.openglu();



            /* Test wrong input    p2=p1  */

            /* Cylinders 5 points */

//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d p3(c7,c8,c9);
//            Vector3d p4(c10,c11,c12);
//            Vector3d p5(c13,c14,c15);
//            Cylinders cy2(p1,p2,p3,p4,p5);
//            cy2.openglu();
//            Sphere C1(p1,0.1);
//            Sphere C2(p2,0.1);
//            Sphere C3(p3,0.1);
//            Sphere C4(p4,0.1);
//            Sphere C5(p5,0.1);
//            C1.openglu();
//            C2.openglu();
//            C3.openglu();
//            C4.openglu();
//            C5.openglu();

            /* Cylinders 2 points, 1 normal */


//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d p3(c7,c8,c9);
//            Vector3d n1(c10,c11,c12);
//            Cylinders c0(p1,n1,p2,p3);
//            std::cout<<c0.dist(p1,0)<<endl;
//            std::cout<<c0.dist(p2,0)<<endl;
//            std::cout<<c0.dist(p3,0)<<endl;
//            c0.openglu();
//            PtCloud pc;
//            pc.addPoint(p1,n1);
//            pc.drawPtCloud();

//            Sphere C2(p2,0.1);
//            Sphere C3(p3,0.1);

//            C2.openglu();
//            C3.openglu();
//            c0.dist(p1,0);



            /* Cylinders 5 points */

//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d p3(c7,c8,c9);
//            Vector3d p4(c10,c11,c12);
//            Vector3d p5(c13,c14,c15);
//            Cylinders cy2(p1,p2,p3,p4,p5);
//            cy2.openglu();
//            Sphere C1(p1,0.1);
//            Sphere C2(p2,0.1);
//            Sphere C3(p3,0.1);
//            Sphere C4(p4,0.1);
//            Sphere C5(p5,0.1);
//            C1.openglu();
//            C2.openglu();
//            C3.openglu();
//            C4.openglu();
//            C5.openglu();
//            std::cout<<cy2.dist(p1,0)<<endl;
//            std::cout<<cy2.dist(p2,0)<<endl;
//            std::cout<<cy2.dist(p3,0)<<endl;
//            std::cout<<cy2.dist(p4,0)<<endl;
//            std::cout<<cy2.dist(p5,0)<<endl;
//            std::cout<<cy2.dist(p1,1)<<endl;


            /* Cones 2 points with normals*/

//            ifstream fichier2("p1.txt", ios::in);                                              // open of the file
//            if(fichier2)
//            {

//                    fichier2>>c1>>c2>>c3;
//                    fichier2.close();

//            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }
//            ifstream fichier3("p2.txt", ios::in);                                              // open of the file
//            if(fichier3)
//            {

//                    fichier3>>c4>>c5>>c6;
//                    fichier3.close();

//            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }
//            ifstream fichier4("n1.txt", ios::in);                                              // open of the file
//            if(fichier4)
//            {

//                    fichier4>>c7>>c8>>c9;
//                    fichier4.close();

//            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }
//            ifstream fichier5("n2.txt", ios::in);                                              // open of the file
//            if(fichier5)
//            {

//                    fichier5>>c10>>c11>>c12;
//                    fichier5.close();

//            }

//            else{
//                    cerr << "Error, can open the file !" << endl;
//            }



//             //reading in different files

//            std::string Buffer = ""; //Variable contenant le texte à réécrire dans le fichier
//            std::ifstream ReadFile( "p1.txt");
//            if (ReadFile) //Si le fichier est trouvé
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile, line ) ) //on parcours le fichier et on initialise line à la ligne actuelle
//                {
//                    Line++;
//                    if(Line != 1) //Si la ligne atteinte est différente de la ligne à supprimer...
//                        Buffer += line + "\n"; //On ajoute le contenu de la ligne dans le contenu à réécrire
//                }
//            }
//            ReadFile.close(); //On ferme le fichier en lecture



//            std::ofstream WriteFile( "p1.txt" );
//            WriteFile << Buffer; //On écris le texte dedans

//            WriteFile.close(); //et on ferme le fichier


//            //-------------------------------------------------------------------
//            std::string Buffer2 = "";
//            std::ifstream ReadFile2( "p2.txt");
//            if (ReadFile2)
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile2, line ) )
//                {
//                    Line++;
//                    if(Line != 1)
//                        Buffer2 += line + "\n";
//                }
//            }
//            ReadFile2.close();

//            std::ofstream WriteFile2( "p2.txt" );
//            WriteFile2 << Buffer2;
//            WriteFile2.close();
//            //-------------------------------------------------------------------
//            std::string Buffer3 = "";
//            std::ifstream ReadFile3( "n1.txt");
//            if (ReadFile3)
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile3, line ) )
//                {
//                    Line++;
//                    if(Line != 1)
//                        Buffer3 += line + "\n";
//                }
//            }
//            ReadFile3.close();

//            std::ofstream WriteFile3( "n1.txt" );
//            WriteFile3 << Buffer3;
//            WriteFile3.close();

//            //-------------------------------------------------------------------
//            std::string Buffer4 = "";
//            std::ifstream ReadFile4( "n2.txt");
//            if (ReadFile4)
//            {
//                std::string line;
//                int Line = 0;
//                while ( std::getline( ReadFile4, line ) )
//                {
//                    Line++;
//                    if(Line != 1)
//                        Buffer4 += line + "\n";
//                }
//            }
//            ReadFile4.close();

//            std::ofstream WriteFile4( "n2.txt" );
//            WriteFile4 << Buffer4;
//            WriteFile4.close();











//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d n1(c7,c8,c9);
//            Vector3d n2(c10,c11,c12);
////            std::cout<<p1<<endl;
////            Vector3d p3(c13,c14,c15);
////            Vector3d n3(c16,c17,c18);
//            Cones cn1(p1,n1,p2,n2,"oppo");
////            Cones cn2(p1,n1,p3,n3,"no");
////            Cones cn3(p3,n3,p2,n2,"no");
/////           Cones cn1(p1,n1,p2,n2);
//            cn1.openglu(50);





//            PtCloud pc1;
//            pc1.addPoint(p1,n1);
//            pc1.drawPtCloud();
//            PtCloud pc2;
//            pc2.addPoint(p2,n2);
//            pc2.drawPtCloud();
////            PtCloud pc3;
////            pc3.addPoint(p3,n3);
////            pc3.drawPtCloud();
//           cn1.displayInfo();
//           cn2.displayInfo();
//           cn3.displayInfo();
//            std::cout<<"dist1: "<<cn1.dist(p1,0)<<endl;
//            std::cout<<"dist2: "<<cn1.dist(p1,1)<<endl;
//            std::cout<<"dist3: "<<cn1.dist(p2,0)<<endl;
//            std::cout<<"dist4: "<<cn1.dist(p2,1)<<endl;
//            std::cout<<"dist5: "<<cn1.dist(p3,0)<<endl;
//            std::cout<<"dist5: "<<cn1.dist(p3,1)<<endl;

            /* Cones 3 points 1 point with normal */

//            Vector3d p1(c1,c2,c3);
//            Vector3d p2(c4,c5,c6);
//            Vector3d p3(c7,c8,c9);
//            Vector3d p4(c10,c11,c12);
//            Vector3d n1(c13,c14,c15);
////            Vector3d p5(c16,c17,c18);
//            Cones cn2(p1,n1,p2,p3,p4,"oppo");
//            cn2.openglu(80);
//            PtCloud pc1;
//            pc1.addPoint(p1,n1);
//            pc1.drawPtCloud();
//            Sphere C2(p2,0.1);
//            Sphere C3(p3,0.1);
//            Sphere C4(p4,0.1);
//            C2.openglu();
//            C3.openglu();
//            C4.openglu();
//            cn2.displayInfo();
//            Sphere C5(p5,0.1);
//            C5.openglu();
//            cn2.get(0).distance(p5);
//            std::cout<<"dist1: "<<cn2.dist(p1,0)<<endl;
//            std::cout<<"dist2: "<<cn2.dist(p1,1)<<endl;
//            std::cout<<"dist3: "<<cn2.dist(p2,0)<<endl;
//            std::cout<<"dist4: "<<cn2.dist(p2,1)<<endl;
//            std::cout<<"dist5: "<<cn2.dist(p3,0)<<endl;
//            std::cout<<"dist6: "<<cn2.dist(p3,1)<<endl;
//            std::cout<<"dist7: "<<cn2.dist(p4,0)<<endl;
//            std::cout<<"dist8: "<<cn2.dist(p4,1)<<endl;
//            std::cout<<"dist5: "<<cn2.dist(p5,0)<<endl;
//            std::cout<<"dist5: "<<cn2.dist(p5,1)<<endl;






            /* Sphere*/


            /* Constructor 1 */
//          Vector3d center(0,0,0);
//          Vector3d p(0,0,0.5);
//          double radius=0.5;
//          Sphere s1(center,radius);
//          s1.openglu(50);
//          s1.displayInfo();

            /* Constructor 2 */

//          Vector3d a(0,0,0.5);
//          Vector3d b(0.5,0,0);
//          Vector3d c(0,0.5,0);
//          Vector3d d(sqrt(2)/4,0,-sqrt(2)/4);
//          Sphere s2(a,b,c,d);
//          s2.openglu();
//          Sphere ca(a,0.1);
//          Sphere cb(b,0.1);
//          Sphere cc(c,0.1);
//          Sphere cd(d,0.1);
//          ca.openglu();
//          cb.openglu();
//          cc.openglu();
//          cd.openglu();

            /* Constructor 3 */

//          Vector3d p1(0,1,0);
//          Vector3d p2(1,0,0);
//          Vector3d n1(1,0,0);
//          Sphere s3(p1,n1,p2);
//          s3.openglu();
//          PtCloud pc;
//          pc.addPoint(p1,n1);
//          pc.drawPtCloud();

            /* Test Cylinder*/


            /* Constructor 1 */

//          Vector3d centre(0,0,0);
//          Vector3d axis(1,0,0);
//          double radius = 0.5;
//          Cylinder cl(centre,axis,radius);
//          cl.openglu(50);
//          cl.displayInfo();
//          Sphere C2(centre,0.1);

//          C2.openglu();
//          Vector3d p(1,1,1);
//          cl.distance(p);

            /* Constructor 2 */

//          Vector3d centre(0,0,0);
//          Vector3d axis(1,0,0);
//          double radius = 0.5;
//          double l1=2;
//          double l2=1;
//          Cylinder C2(centre,axis,radius,l1,l2);
//          C2.openglu(50);

            /* Constructor 3 */

//          Vector3d v1(1,0,0);
//          Vector3d v2(0,1,0);
//          Vector3d v3(0,1,-1);
//          Cylinder C3(v1,v2,v3);
//          C3.openglu();

            /* Exemple Cylinders*/

            /* Constructor 1 */
            /* Compute 0 cylinder */

//          Vector3d p1(1,0,0);
//          Vector3d p2(1,0,1);
//          Vector3d p3(1,0,2);
//          Vector3d n1(1,0,0);
//          Cylinders c0(p1,n1,p2,p3);
//          c0.openglu();
//          PtCloud pc;
//          pc.addPoint(p1,n1);
//          pc.drawPtCloud();

//          Sphere C2(p2,0.1);
//          Sphere C3(p3,0.1);

//          C2.openglu();
//          C3.openglu();

            /* Compute 2 cylinders */

//          Vector3d p1(1,0,0);
//          Vector3d p2(0,1,0);
//          Vector3d p3(sqrt(2)/2,-sqrt(2)/2,-1);
//          Vector3d n1(1,0,0);
//          Cylinders C1(p1,n1,p2,p3);
//          C1.openglu();
//          C1.displayInfo();
//          PtCloud pc;
//          pc.addPoint(p1,n1);
//          pc.drawPtCloud();
//          Sphere C2(p2,0.1);
//          Sphere C3(p3,0.1);

//          C2.openglu();
//          C3.openglu();

            /* Constructor 2 */
            /* Compute 0 cylinder */

//          Vector3d v1(1,0,0);
//          Vector3d v2(0,1,0);
//          Vector3d v3(0,1,-1);
//          Cylinders cy0(v1,v2,v3,v1+v2,v3);
//          cy0.openglu();

            /* Compute 2 cylinders*/

//          Vector3d a(1,0,0);
//          Vector3d b(0,1,0);
//          Vector3d c(sqrt(2)/2,sqrt(2)/2,0);
//          Vector3d d(-sqrt(2)/2,sqrt(2)/2,-1);
//          Vector3d e(0,1,1);
//          Vector3d a(0,0,0);
//          Vector3d b(1,0,0);
//          Vector3d c(0,2,0);
//          Vector3d d(1,0,-1);
//          Vector3d e(0,0,1);
//          Cylinders cy1(a,b,c,d,e);
//          cy1.openglu();

            /* Compute 4 cylinders */

//          Vector3d a(1,0,0);
//          Vector3d b(0,1,0);
//          Vector3d c(sqrt(2)/2,sqrt(2)/2,0);
//          Vector3d d(-sqrt(2)/2,sqrt(2)/2,-1);
//          Vector3d e(0,0,0);
//          Cylinders cy2(a,b,c,d,e);
//          cy2.openglu();
//          Sphere sa(a,0.1);
//          Sphere sb(b,0.1);
//          Sphere sc(c,0.1);
//          Sphere sd(d,0.1);
//          Sphere se(e,0.1);
//          sa.openglu();
//          sb.openglu();
//          sc.openglu();
//          sd.openglu();
//          se.openglu();


            /* Exemple Plane */

            /* Constructor 1 */

//          Vector3d p1(0,0,0);
//          Vector3d p2(1,0,0);
//          Vector3d p3(0,0,1);
//          Plane p(p1,p2,p3);
//          p.openglu(50);
//          p.displayInfo();
//          Sphere C1(p1,0.1);
//          Sphere C2(p2,0.1);
//          Sphere C3(p3,0.1);
//          C1.openglu();
//          C2.openglu();
//          C3.openglu();
//          p.distance(p1);


            /* Constructor 2 */

//          Vector3d p1(1,1,1);
//          Vector3d p2(-1,-1,0);
//          Vector3d p3(1,0,0);
//          Plane p(p1,p2,p3);
//          p.openglu();


//            std::cout<<"Test P1 is on the Plane"<<endl;
//            p.distance(p1);
//            std::cout<<endl;
//            std::cout<<"Test P2 is on the Plane"<<endl;
//            p.distance(p2);

//            std::cout<<"-------------------------------"<<endl;

}




void Viewer::init()
{
  // Restore previous viewer state.
 // restoreStateFromFile();

  // Opens help window
  // help();
}

QString Viewer::helpString() const
{
  QString text("<h2>S i m p l e V i e w e r</h2>");
  text += "Use the mouse to move the camera around the object. ";
  text += "You can respectively revolve around, zoom and translate with the three mouse buttons. ";
  text += "Left and middle buttons pressed together rotate around the camera view direction axis<br><br>";
  text += "Pressing <b>Alt</b> and one of the function keys (<b>F1</b>..<b>F12</b>) defines a camera keyFrame. ";
  text += "Simply press the function key again to restore it. Several keyFrames define a ";
  text += "camera path. Paths are saved when you quit the application and restored at next start.<br><br>";
  text += "Press <b>F</b> to display the frame rate, <b>A</b> for the world axis, ";
  text += "<b>Alt+Return</b> for full screen mode and <b>Control+S</b> to save a snapshot. ";
  text += "See the <b>Keyboard</b> tab in this window for a complete shortcut list.<br><br>";
  text += "Double clicks automates single click actions: A left button double click aligns the closer axis with the camera (if close enough). ";
  text += "A middle button double click fits the zoom of the camera and the right button re-centers the scene.<br><br>";
  text += "A left button double click while holding right button pressed defines the camera <i>Revolve Around Point</i>. ";
  text += "See the <b>Mouse</b> tab and the documentation web pages for details.<br><br>";
  text += "Press <b>Escape</b> to exit the viewer.";
  return text;
}
