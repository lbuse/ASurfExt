#ifndef PLANE_H
#define PLANE_H

#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <math.h>
#include <Shape.h>
#include "gl.h"
#include "glu.h"
using namespace Eigen;
using namespace std;

class Plane// : public Shape
{

private:
    //**Attributes
    Vector3d normal;    //* direction of the normal of the plane
    Vector3d origin;    //* the point of intersection between the plane and the normal

public:
    //**Constructors
    Plane();
    Plane(const Vector3d&, const Vector3d&, const Vector3d&);   //* 3 points define 1 plane
    Plane(const Vector3d&, const Vector3d&);                    //* 1 point and 1 normal define 1 plane
	
    //**Members
    void distance( Vector3d);                             //* Print the Distance between a point and the Plane
    void displayInfo();                                         //* Displays the infos of the Plane in the console
    void openglu(int=50);                                       //* Draws the Plane inside an OpenGL window
    double dist(Vector3d);                                      //* Distance between a point and the Plane
};
#endif
