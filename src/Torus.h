#ifndef TORUS_H
#define TORUS_H

#include <Eigen/Dense>
//#include <QGLViewer/qglviewer.h>
#include <vector>
#include <Sphere.h>
#include <Shape.h>
#include <iostream>
using namespace Eigen;
using namespace std;

class Torus// : public Shape
{

private:
    //**Attributes
    Vector3d center;
    Vector3d axis;
    double large_radius;
    double small_radius;
    double angle_start;
    double angle_end;

public:
    //**Constructors
    Torus();
    Torus(const Vector3d&,const Vector3d&,double,double, double=0, double=0);   //* center, axis, large_radius, small_radius, angle_min, angle_max

    //**Members
    void distance( Vector3d);                   //* Plot the Distance between a point and the Torus
    void displayInfo();                         //* Displays the infos of the Torus in the console
    void openglu(int = 50);                     //* Draws the Torus inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    double dist(Vector3d);                      //* Compute the distcance betwenn a point and the Torus
    double normal_deviation(Vector3d , Vector3d );

};
#endif
