#include "Cylinders.h"
#include "Cylinder.h"
#include "R_ext/Lapack.h"
#define THRESHOLD 0.0001

typedef Matrix<double, 12, 12> Matrix12d;
typedef Matrix<double, 10, 10> Matrix10d;

Cylinders::
    Cylinders() {
    }

// Mixed Minimal Point Set
Cylinders::
    Cylinders(const Vector3d& p1, const Vector3d& n, const Vector3d& p2, const Vector3d& p3){

        // Test if 2 points are equals
        if (((p1-p2).norm() < THRESHOLD) || ((p1-p3).norm() < THRESHOLD) ||((p2-p3).norm() < THRESHOLD)){
            std::cerr<< "2 points are equals"<< endl;
            return;
        }
        // Test if the Normal is not 0 0 0
        if ((abs(n(0))<THRESHOLD) && (abs(n(1))<THRESHOLD) && (abs(n(2))<THRESHOLD)){
            std::cerr<< "Normal is undefined"<< endl;
            return;
        }
        Vector3d n1 = n.normalized();
        Matrix3d Rotation;
        double rho = sqrt(n1(1)*n1(1)+n1(2)*n1(2));
        double t = norm(n1);
        Matrix3d Rotation2;
        Rotation2 << rho/t,     -n1(0)*n1(1)/(t*rho), -n1(0)*n1(2)/(t*rho),
                     0,         n1(2)/rho,             -n1(1)/rho,
                     n1(0)/t,  n1(1)/t,               n1(2)/t;
        if (fabs(rho)<THRESHOLD) {
            rho = sqrt(n1(0)*n1(0)+n1(1)*n1(1));
            Rotation2 <<    -n1(2)*n1(0)/(t*rho),   -n1(2)*n1(1)/(t*rho),   rho/t,
                            n1(1)/rho,              -n1(0)/rho,             0,
                            n1(0)/t,                n1(1)/t,                n1(2)/t;
        }
        Rotation = Rotation2.transpose();
        Vector3d p_1 = Rotation2*(p1-p1);   // p1(0,0,0)
        Vector3d p_2 = Rotation2*(p2-p1);   // p2(x,y,z)
        Vector3d p_3 = Rotation2*(p3-p1);   // p3(x,y,z)

        if (p_2(2)*p_3(2) < 0) {    // No solution

            return;
        }

        double a = (p_2(1)*p_2(1)+p_2(2)*p_2(2))*p_3(2) - (p_3(1)*p_3(1)+p_3(2)*p_3(2))*p_2(2);
        double b = -2*(p_2(0)*p_2(1)*p_3(2) - p_3(0)*p_3(1)*p_2(2));
        double c = (p_2(0)*p_2(0)+p_2(2)*p_2(2))*p_3(2) - (p_3(0)*p_3(0)+p_3(2)*p_3(2))*p_2(2);

        if ((a == 0) && (b == 0) && (c == 0)) {   // Infinite solutions

            return;
        }
        double l;
        double m;
        double r;
        Vector3d v;
        /* Different cases */
        if (a == 0) {
            if ((c!=0)) {
                l=1;
                m=-b/c;
                v << l,m,0;
                if (p_2(2)!=0) {
                    r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
                if (p_3(2)!=0) {
                    r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
            }
            l=1;
            m=0;
            v << l,m,0;
            if (p_2(2)!=0) {
                r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
            if (p_3(2)!=0) {
                r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
        }
        else if (c == 0) {
            m=1;
            l=-b/a;
            v << l,m,0;
            if (p_2(2)!=0) {
                r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
            if (p_3(2)!=0) {
                r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
            l=0;
            m=1;
            v << l,m,0;
            if (p_2(2)!=0) {
                r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
            if (p_3(2)!=0) {
                r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                r=fabs(r);

                Vector3d center(0,0,-r);
                double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
            }
        }
        else {
            double delta = b*b-4*a*c;
            if (delta == 0){
                m = 1;
                l = -b/(2*a);
                v << l,m,0;
                if (p_2(2)!=0) {
                    r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
                else if (p_3(2)!=0) {
                    r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }

            }
            else if (delta > 0) {
                m = 1;
                l = (- b + sqrt(delta)) / (2 * a);
                v << l,m,0;
                if (p_2(2)!=0) {
                    r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
                else if (p_3(2)!=0) {
                    r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
                l = (- b - sqrt(delta)) / (2 * a);
                v << l,m,0;
                if (p_2(2)!=0) {
                    r = 1/(2*p_2(2)) * (1/(l*l+m*m) * (-p_2(0)*m+p_2(1)*l) * (-p_2(0)*m+p_2(1)*l) + p_2(2)*p_2(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
                else if (p_3(2)!=0) {
                    r = 1/(2*p_3(2)) * (1/(l*l+m*m) * (-p_3(0)*m+p_3(1)*l) * (-p_3(0)*m+p_3(1)*l) + p_3(2)*p_3(2));
                    r=fabs(r);

                    Vector3d center(0,0,-r);
                    double l1=min(dotProduct(p_1-center,v.normalized()),min(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    double l2=max(dotProduct(p_1-center,v.normalized()),max(dotProduct(p_2-center,v.normalized()),dotProduct(p_3-center,v.normalized())));
                    this->list.push_back(Cylinder(p1+sign(p_2(2))*r*n1,Rotation*v,r,l1,l2));
                }
            }
        }
    }

// Five Points
Cylinders::
    Cylinders(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3, const Vector3d& p4, const Vector3d& p5){
    // Test if 2 points are equals
    if (((p1-p2).norm() < THRESHOLD) || ((p1-p3).norm() < THRESHOLD) || ((p1-p4).norm() < THRESHOLD)|| ((p1-p5).norm() < THRESHOLD)|| ((p2-p3).norm() < THRESHOLD)|| ((p2-p4).norm() < THRESHOLD)|| ((p2-p5).norm() < THRESHOLD)|| ((p3-p4).norm() < THRESHOLD)|| ((p3-p5).norm() < THRESHOLD)|| ((p4-p5).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
    Vector3d nx = (p2-p1).normalized();
    Vector3d ny = ((p3-p1)-dotProduct(p3-p1,nx)*nx).normalized();
    Vector3d nz = crossProduct(nx,ny).normalized();
    Matrix3d Rotation;
    Matrix3d Rotation2;
    Rotation << nx(0),ny(0),nz(0),
                nx(1),ny(1),nz(1),
                nx(2),ny(2),nz(2);
    Rotation2 = Rotation.transpose();
    Vector3d p_1 = Rotation2*(p1-p1);   // p1(0,0,0)
    Vector3d p_2 = Rotation2*(p2-p1);   // p2(x,0,0)
    Vector3d p_3 = Rotation2*(p3-p1);   // p3(x,y,0)
    Vector3d p_4 = Rotation2*(p4-p1);   // p4(x,y,z)
    Vector3d p_5 = Rotation2*(p5-p1);   // p5(x,y,z)

    if ((abs(p_3(1)) < THRESHOLD) && (abs(p_3(0)) > THRESHOLD) ){ // p_1 p_2 p_3 are alligned so infinite or no cylinder

        return;
    }        

    if ((abs(p_4(2)) < THRESHOLD) && (abs(p_5(2)) < THRESHOLD)) {
;
        return;
    }
    if (abs(p_4(2)) < THRESHOLD){      // permutaion in order to have z!=0 for p5
        Vector3d ptemp = p_5;
        p_5 = p_4;
        p_4 = ptemp;
    }

    int N = 12;

    Matrix12d Aprovisoire;  //* Companion matrix

    Aprovisoire <<
            0,  0,  0,  0,  0,  0,  0,  0,  -p_2(0)*p_3(1)*(-p_3(1)*p_4(1)+power(p_4(1),2)+power(p_4(2),2)),
                                                -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_4(0)*p_3(1)*p_4(2),
                                                    -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)+p_2(0)*power(p_3(1),2)*p_4(1)-p_2(0)*p_3(1)*power(p_4(1),2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  -p_2(0)*p_3(1)*(-p_3(1)*p_4(1)+power(p_4(1),2)+power(p_4(2),2)),
                                                    -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_4(0)*p_3(1)*p_4(2),
                                                        -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)+p_2(0)*power(p_3(1),2)*p_4(1)-p_2(0)*p_3(1)*power(p_4(1),2),
            0,  0,  0,  0,  0,  0,  0,  0,  power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                2*p_4(0)*p_3(1)*p_4(2)*p_5(2)-2*p_5(0)*p_3(1)*p_4(2)*p_5(2),
                                                    -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)+power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                    2*p_4(0)*p_3(1)*p_4(2)*p_5(2)-2*p_5(0)*p_3(1)*p_4(2)*p_5(2),
                                                        -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)+power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2),
            1,  0,  0,  0,  0,  0,  0,  0,  -p_4(2)*power(p_3(1),2)*p_2(0),
                                                -2*p_2(0)*p_3(0)*p_3(1)*p_4(1)+2*p_2(0)*p_4(0)*p_3(1)*p_4(1),
                                                    power(p_2(0),2)*p_3(0)*p_4(2)-p_2(0)*power(p_3(0),2)*p_4(2)-p_4(2)*power(p_3(1),2)*p_2(0)+2*p_2(0)*p_3(1)*p_4(1)*p_4(2),
                                                        0,
            0,  1,  0,  0,  0,  0,  0,  0,  0,  -p_4(2)*power(p_3(1),2)*p_2(0),
                                                    -2*p_2(0)*p_3(0)*p_3(1)*p_4(1)+2*p_2(0)*p_4(0)*p_3(1)*p_4(1),
                                                        power(p_2(0),2)*p_3(0)*p_4(2)-p_2(0)*power(p_3(0),2)*p_4(2)-p_4(2)*power(p_3(1),2)*p_2(0)+2*p_2(0)*p_3(1)*p_4(1)*p_4(2),
            0,  0,  1,  0,  0,  0,  0,  0,  0,  -2*p_3(0)*p_3(1)*p_4(1)*p_5(2)+2*p_3(0)*p_3(1)*p_5(1)*p_4(2)+2*p_4(0)*p_3(1)*p_4(1)*p_5(2)-2*p_5(0)*p_3(1)*p_5(1)*p_4(2),
                                                    2*p_3(1)*p_4(1)*p_4(2)*p_5(2)-2*p_3(1)*p_5(1)*p_4(2)*p_5(2),
                                                        0,
            0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  -2*p_3(0)*p_3(1)*p_4(1)*p_5(2)+2*p_3(0)*p_3(1)*p_5(1)*p_4(2)+2*p_4(0)*p_3(1)*p_4(1)*p_5(2)-2*p_5(0)*p_3(1)*p_5(1)*p_4(2),
                                                        2*p_3(1)*p_4(1)*p_4(2)*p_5(2)-2*p_3(1)*p_5(1)*p_4(2)*p_5(2),
            0,  0,  0,  0,  1,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_3(0)*p_3(1)*p_4(2),
                                                    -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)-p_2(0)*p_3(1)*power(p_4(2),2),
                                                        0,
            0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_3(0)*p_3(1)*p_4(2),
                                                        -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)-p_2(0)*p_3(1)*power(p_4(2),2),
            0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2);

    Matrix12d Bprovisoire;  //* Companion matrix

    Bprovisoire <<
            1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(0)*p_4(2)+p_2(0)*power(p_3(0),2)*p_4(2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(0)*p_4(2)+p_2(0)*power(p_3(0),2)*p_4(2),
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0;

    double A[N*N];  //* array
    double B[N*N];  //* array
    for (int j=0; j<N; j++){
        for(int i=0; i<N; i++){
            A[i+j*N]=Aprovisoire(i,j);
            B[i+j*N]=Bprovisoire(i,j);
        }
    }

    double alphar[N];   // alpha's real part
    double alphai[N];   // alpha's imaginary part
    double beta[N];     // beta : eigenvalues lambda_i = (alphar_i+i*alphai_i)/beta_i
    double vl[N*N];     // left eigenvectors (useless)
    double vr[N*N];     // right eigenvectors
    char compute_l='N';  // compute (V) or not (N) the left eigenvectors
    char compute_r='V';  // compute (V) or not (N) the right eigenvectors
    double work[16*N];  // Working variable (minimum size = 8*n)
    int lwork=16*N;     // work's length
    int info;           // information about the success of the calculation

    //  DGGEV computes for a pair of N-by-N real nonsymmetric matrices (A,B)
    //  the generalized eigenvalues, and optionally, the left and/or right
    //  generalized eigenvectors. (lapack)

    dggev_(&compute_l, &compute_r,
           &N, A, &N, B, &N,
           alphar, alphai, beta,
           vl, &N, vr, &N,
           work, &lwork, &info);
    // VR_i associated a lambda_i : vr[(0->N-1)+i*N]
    Vector3d center_bar;
    Vector3d center;
    Vector3d axis;
    double r0;

    // Generating the cones correspondinng to the real non-infinite eigenvalues
    for (int i=0; i<N; i++){
        if(beta[i]!=0 && alphai[i]==0){
            axis = Vector3d(vr[N-2+i*N]/vr[N-1+i*N],alphar[i]/beta[i],1).normalized();
            Vector3d a = p_1 - dotProduct(p_1,axis)*axis;
            Vector3d b = p_2 - dotProduct(p_2,axis)*axis;
            Vector3d c = p_3 - dotProduct(p_3,axis)*axis;

            // 'lx' is the opposite lenght to 'x'
            double la = norm(b-c);
            double lb = norm(c-a);
            double lc = norm(a-b);
            double sa = .5*(-la*la + lb*lb + lc*lc);
            double sb = .5*( la*la - lb*lb + lc*lc);
            double sc = .5*( la*la + lb*lb - lc*lc);

            center_bar = Vector3d(la*la*sa,lb*lb*sb,lc*lc*sc);
            center_bar = center_bar / (center_bar(0)+center_bar(1)+center_bar(2));

            center = center_bar(0)*a + center_bar(1)*b + center_bar(2)*c;

            r0 = sqrt((la*la*lb*lb*lc*lc) / (4*power(norm(crossProduct(b-a,c-a)),2)));

            double l1=min(dotProduct(p_1-center,axis),min(dotProduct(p_2-center,axis),min(dotProduct(p_3-center,axis),min(dotProduct(p_4-center,axis),dotProduct(p_5-center,axis)))));
            double l2=max(dotProduct(p_1-center,axis),max(dotProduct(p_2-center,axis),max(dotProduct(p_3-center,axis),max(dotProduct(p_4-center,axis),dotProduct(p_5-center,axis)))));

            this->list.push_back(Cylinder(p1+Rotation*center,Rotation*axis,r0,l1,l2));
        }
    }
}



Cylinder Cylinders::
    get(int i){
        return this->list[i];
    }

unsigned int Cylinders::
    size(){
        return this->list.size();
    }

void Cylinders::
    displayInfo(){
    std::cout<<"\nHere are the cylinders : \n";
        for (unsigned int i=0; i<size(); i++){
            std::cout<<"-----------------------\n"<<i+1<<"\n-------------\n";
            this->list[i].displayInfo();
        }
    }

void Cylinders::
    openglu(int precision){
        for (unsigned int i=0; i<size(); i++){
            if(i==0){glColor3f(1.0f,1.0f,0.0f);}
            else if(i==1){glColor3f(1.0f,0.0f,0.0f);}
            else if(i==2){glColor3f(0.0f,1.0f,1.0f);}
            else if(i==3){glColor3f(0.0f,1.0f,0.0f);}
            else if(i==4){glColor3f(0.0f,0.0f,1.0f);}
            else if(i==5){glColor3f(1.0f,0.0f,1.0f);}
            else if(i==6){glColor3f(0.25f,0.75f,0.0f);}
            else if(i==7){glColor3f(0.75f,0.25f,0.0f);}
            else{glColor3f(0.5f,0.5f,1.0f);}
            this->list[i].openglu(precision);
        }
    }

double Cylinders::
    dotProduct(Vector3d v1, Vector3d v2){
    return v1(0)*v2(0)+v1(1)*v2(1)+v1(2)*v2(2);
}

Vector3d Cylinders::
    crossProduct(Vector3d v1, Vector3d v2){
    return Vector3d(v1(1)*v2(2)-v1(2)*v2(1),
                    v1(2)*v2(0)-v1(0)*v2(2),
                    v1(0)*v2(1)-v1(1)*v2(0));
}

double Cylinders::
    norm(Vector3d v){
    return sqrt(dotProduct(v,v));
}

int Cylinders::
    sign(double d){
    if(d>0){
        return +1;
    }
    return -1;
}

double Cylinders::
    power(double d, int p){
    double value = d;
    for(int i=1; i<p; i++){
        value = value * d;
    }
    return value;
}

double Cylinders::
    dist(Vector3d p,int i){
    return this->list[i].dist(p);
}
