#include "Sphere.h"
#include <iostream>
#include <fstream>
#define THRESHOLD 0.0001
Sphere::
    Sphere() {
    }

Sphere::
    Sphere(const Vector3d &c , const double& r) {
    if(abs(r) < THRESHOLD){
        std::cerr<< "radius is lower than 0.0001"<< endl;
    }
    this->center = c;
    this->radius = r;
}

Sphere::
    Sphere(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3, const Vector3d& p4) {

    // Test if 2 points are equals
    if (((p1-p2).norm() < THRESHOLD) || ((p1-p3).norm() < THRESHOLD) || ((p1-p4).norm() < THRESHOLD)|| ((p2-p3).norm() < THRESHOLD)|| ((p2-p4).norm() < THRESHOLD)|| ((p3-p4).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
    Matrix3d A;
	A << (p2-p1).transpose(), (p3-p2).transpose(), (p4-p3).transpose();
    Vector3d b;
    b << 0.5*(p2.squaredNorm()-p1.squaredNorm()),
	     0.5*(p3.squaredNorm()-p2.squaredNorm()),
             0.5*(p4.squaredNorm()-p3.squaredNorm());
	this->center = A.inverse()*b;
	this->radius = (p1-this->center).norm();  
}

Sphere::
    Sphere(const Vector3d& p1, const Vector3d& N, const Vector3d& p2) {

    // Test if the Normal is not 0 0 0
    if ((abs(N(0))<THRESHOLD) && (abs(N(1))<THRESHOLD) && (abs(N(2))<THRESHOLD)){
        std::cerr<< "Normal is undefined"<< endl;
        return;
    }
    // Test if 2 points are equals
    if ((p1-p2).norm() < THRESHOLD) {
        std::cerr<< "2 points are equals"<< endl;
        return;
    }

    Matrix4d A;
	A << 1, 0, 0, N(0)/N.norm(),
	     0, 1, 0, N(1)/N.norm(),
	     0, 0, 1, N(2)/N.norm(),
	     (p2-p1)(0), (p2-p1)(1), (p2-p1)(2), 0;
    Vector4d b, s;
	b << p1(0),p1(1),p1(2),0.5*(p2.squaredNorm()-p1.squaredNorm());
	s = A.inverse()*b;
	this->center << s(0), s(1), s(2);
    this->radius = fabs(s(3));
}
double Sphere::
    dist( Vector3d vector){
    return std::abs( (vector-this->center).norm()-this->radius );
}

void Sphere::
    distance( Vector3d vector) {
    double distance = dist (vector);
    if (distance <= THRESHOLD) {
        std::cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to inlier !"<<std::endl;
	} else {
        std::cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to outlier !"<<std::endl;
 	}
}

void Sphere::
    displayInfo() {
    cout<<"The center of current sphere is:\n"
	    <<this->center<<endl;
    cout<<"The implicit equation is:\n"
	    <<"(x - "<<this->center(0)<<")² + "
	    <<"(y - "<<this->center(1)<<")² + "
	    <<"(z - "<<this->center(2)<<")² = "
	    <<pow(this->radius, 2)<<endl;
    cout<<center<<"\n--\n"<<radius<<endl;
}


void Sphere::
     openglu(int precision ) {
          GLUquadric *quad;
          quad = gluNewQuadric();
          gluQuadricDrawStyle(quad,GLU_LINE);
          glMatrixMode(GL_MODELVIEW);
          glPushMatrix();
          glTranslatef(this->center(0),this->center(1),this->center(2));
          gluSphere(quad,this->radius,precision,precision);
          glPopMatrix();
          gluDeleteQuadric(quad);
          glBegin(GL_QUADS);
          for (int i=-precision; i<=precision; i++) {
              double z = this->center(2) + this->radius*
                         sin(static_cast<double>(i)/precision*my_pi*0.5);
              for (int j=0; j<=precision; j++) {
                  double x = this->center(0) + this->radius*
                             cos(static_cast<double>(j)/precision*2*my_pi)*
                             cos(static_cast<double>(i)/precision*my_pi*0.5);
                  double y = this->center(1) + this->radius*
                             sin(static_cast<double>(j)/precision*2*my_pi)*
                             cos(static_cast<double>(i)/precision*my_pi*0.5);
               glColor3f(x, y, z);
               glNormal3f(x, y, z);
               glVertex3f(x, y, z);
              }
           glEnd();
           }






}
