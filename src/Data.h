#ifndef DATA_H
#define DATA_H

#include <Eigen/Dense>
#include <list>
#include <vector>
#include "gl.h"
#include "glu.h"
class PtCloud
{


public:
    struct point_with_normal{                         //* structure containing both the coordinates of a point and its normal
        Eigen::Vector3d p;                            //* vector3d corresponding to the coordinates of the point
        Eigen::Vector3d n;                            //* vector3d corresponding to the coordinates of the normal of the point
    };
    //**Constructors
    PtCloud() ;
    //**Members
    void addPoint(Eigen::Vector3d,Eigen::Vector3d);   //* add a point and its normal in the structure  (list)

    int getSize() const;                              //* return number of points

    Eigen::Vector3d getPoint(int) const;              //*  arg:i   return point  row i

    Eigen::Vector3d getNormal(int) const;             //* arg: i   return normal  row i

    point_with_normal back();                         //* return last element of the list : point + normal
    bool empty();                                     //* test if the list is empty
    void push_back(point_with_normal);                //* add a pair point/normal to the current list
    void pop_back();                                  //* remove the last point and normal of the list





    void drawPtCloud() const;                         //* draw the points and normals


private:
    std::vector<point_with_normal> ptlist;            //* list containing the structure point/normal
};

#endif

