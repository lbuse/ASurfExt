#include "Cone.h"
#include "math.h"
#include "R_ext/Lapack.h"
#include <string>
#include <sstream>
using namespace std;
#define THRESHOLD 0.0001

Cone::
    Cone(){

    }



Cone::
    Cone(const Vector3d& p, const Vector3d& v, double a, double l1, double l2){
    // Test if the Normal is not 0 0 0
    if ((abs(v(0))<THRESHOLD) && (abs(v(1))<THRESHOLD) && (abs(v(2))<THRESHOLD)){
        std::cerr<< "axis is undefined"<< endl;
        return;
    }
    // Test if the angle is not null
    if ((abs(a)<THRESHOLD)){
        std::cerr<< "angle is null"<< endl;
        return;
    }
        this->apex=p;
        this->axis=v;
        this->angle=a;
        this->length_max=max(l1,l2);
        this->length_min=min(l1,l2);
    }



double Cone::
    dotProduct(Vector3d v1, Vector3d v2){
    return v1(0)*v2(0)+v1(1)*v2(1)+v1(2)*v2(2);
}

Vector3d Cone::
    crossProduct(Vector3d v1, Vector3d v2){
    return Vector3d(v1(1)*v2(2)-v1(2)*v2(1),
                    v1(2)*v2(0)-v1(0)*v2(2),
                    v1(0)*v2(1)-v1(1)*v2(0));
}

double Cone::
    norm(Vector3d v){
    return sqrt(dotProduct(v,v));
}

double Cone::
    dist(Vector3d p){
    double teta; // radian angle between apex-p and the axis
    teta=std::acos(dotProduct(axis,p-apex)/(norm(axis)*norm(p-apex)));

    if (abs (teta+angle-M_PI)<0.000001){ // teta and angle: supplementary angles
        teta=angle;

    }
    else if ((abs(teta+angle-M_PI_2)<0.000001) &(length_max*length_min<0.000001)){ // the point is closer to the opposite side of the cone
        teta=M_PI-teta-2*angle;
    }

    return norm(p-apex)*sin(abs(teta-angle));

}
void Cone::
    distance(Vector3d p){
    double distance =dist(p);
    if (distance <= THRESHOLD) {
        cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to inlier !"<<endl;
    } else {
        cout<<"The distance is = "<<distance<<"\n"
        <<"This point belongs to outlier !"<<endl;
    }
}



void Cone::
    displayInfo(){
        std::cout<<"The apex of current cone is:\n"<<this->apex<<std::endl;
        std::cout<<"The axis direction is:\n"<<this->axis<<std::endl;
        std::cout<<"The angle of the cone is:\n"<<this->angle<<std::endl;
    }

void Cone::
    openglu(int precision) {
        GLUquadric *quad;
        if (length_min>0 && length_max>0){
            quad = gluNewQuadric();
            gluQuadricDrawStyle(quad,GLU_LINE);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            Vector3d v;
            if ((this->axis(0)!=0) | (this->axis(1)!=0)){
                v=Vector3d(this->axis(1),-this->axis(0),0);
            }
            else{
                v=Vector3d(0,-this->axis(2),this->axis(1));
            }
            v=v.normalized();
            Vector3d b(v(1)*this->axis(2)-v(2)*this->axis(1),v(2)*this->axis(0)-v(0)*this->axis(2),v(0)*this->axis(1)-v(1)*this->axis(0));
            b=b.normalized();
            GLdouble Rotation[16]={b(0),    v(0),   this->axis(0),   this->apex[0]+this->axis(0)*length_min,
                                   b(1),    v(1),   this->axis(1),   this->apex[1]+this->axis(1)*length_min,
                                   b(2),    v(2),   this->axis(2),   this->apex[2]+this->axis(2)*length_min,
                                   0,       0,      0,      1};
            glMultTransposeMatrixd(Rotation);
            gluCylinder(quad,std::tan( this->  angle)*this->length_min,std::tan( this->  angle)*this->length_max,length_max-length_min,precision,precision);
            glPopMatrix();
            gluDeleteQuadric(quad);
        }
        else if (length_min<0 && length_max<0){
            quad = gluNewQuadric();
            gluQuadricDrawStyle(quad,GLU_LINE);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            Vector3d v;
            if ((this->axis(0)!=0) | (this->axis(1)!=0)){
                v=Vector3d(this->axis(1),-this->axis(0),0);
            }
            else{
                v=Vector3d(0,-this->axis(2),this->axis(1));
            }
            v=v.normalized();
            Vector3d b(v(1)*this->axis(2)-v(2)*this->axis(1),v(2)*this->axis(0)-v(0)*this->axis(2),v(0)*this->axis(1)-v(1)*this->axis(0));
            b=b.normalized();
            GLdouble Rotation[16]={b(0),    v(0),   this->axis(0),   this->apex[0]+this->axis(0)*length_min,
                                   b(1),    v(1),   this->axis(1),   this->apex[1]+this->axis(1)*length_min,
                                   b(2),    v(2),   this->axis(2),   this->apex[2]+this->axis(2)*length_min,
                                   0,       0,      0,      1};
            glMultTransposeMatrixd(Rotation);
            gluCylinder(quad,- std::tan( this->  angle)*this-> length_min,-std::tan(this->angle)*this->length_max,length_max-length_min,precision,precision);
            glPopMatrix();
            gluDeleteQuadric(quad);
        }
        else{
            quad = gluNewQuadric();
            gluQuadricDrawStyle(quad,GLU_LINE);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            Vector3d v;
            if ((this->axis(0)!=0) | (this->axis(1)!=0)){
                v=Vector3d(this->axis(1),-this->axis(0),0);
            }
            else{
                v=Vector3d(0,-this->axis(2),this->axis(1));
            }
            v=v.normalized();
            Vector3d b(v(1)*this->axis(2)-v(2)*this->axis(1),v(2)*this->axis(0)-v(0)*this->axis(2),v(0)*this->axis(1)-v(1)*this->axis(0));
            b=b.normalized();
            GLdouble Rotation[16]={b(0),    v(0),   this->axis(0),   this->apex[0],
                                   b(1),    v(1),   this->axis(1),   this->apex[1],
                                   b(2),    v(2),   this->axis(2),   this->apex[2],
                                   0,       0,      0,      1};
            glMultTransposeMatrixd(Rotation);
            gluCylinder(quad,0,std::tan( this->  angle)*this->length_max,length_max,precision,precision);
            glPopMatrix();
            gluDeleteQuadric(quad);
            quad = gluNewQuadric();
            gluQuadricDrawStyle(quad,GLU_LINE);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            Vector3d axis_minus = - this->axis;
            if ((axis_minus(0)!=0) | (axis_minus(1)!=0)){
                v=Vector3d(axis_minus(1),-axis_minus(0),0);
            }
            else{
                v=Vector3d(0,-axis_minus(2),axis_minus(1));
            }
            v=v.normalized();
            b = Vector3d(v(1)*axis_minus(2)-v(2)*axis_minus(1),v(2)*axis_minus(0)-v(0)*axis_minus(2),v(0)*axis_minus(1)-v(1)*axis_minus(0));
            b=b.normalized();
            GLdouble Rotation2[16]={b(0),    v(0),   axis_minus(0),   this->apex[0],
                                   b(1),    v(1),   axis_minus(1),   this->apex[1],
                                   b(2),    v(2),   axis_minus(2),   this->apex[2],
                                   0,       0,      0,      1};
            glMultTransposeMatrixd(Rotation2);
            gluCylinder(quad,0,-std::tan( this->  angle)*this->length_min,-length_min,precision,precision);
            glPopMatrix();
            gluDeleteQuadric(quad);
        }
    }
double Cone::
    power(double d, int p){
    double value = d;
    for(int i=1; i<p; i++){
        value = value * d;
    }
    return value;
}



double Cone::getL_max(){
    return length_max;
}
double Cone::getL_min(){
    return length_min;
}
double Cone::get_Angle(){
    return angle;
}

