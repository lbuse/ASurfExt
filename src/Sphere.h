#ifndef SPHERE_H
#define SPHERE_H

#include <Eigen/Dense>
#include <fstream>
#include <Shape.h>
#include "gl.h"
#include "glu.h"


using namespace std;
using namespace Eigen;

class Sphere// : public Shape
{

private:
    Vector3d center;    //* coordinates of the center of the sphere
    double radius;      //* radius of the sphere

public:
    //**Contructors
	Sphere();
    Sphere(const Vector3d &,const double &);                    //* From center and radius
    Sphere(const Vector3d&,const Vector3d&,const Vector3d&,
           const Vector3d&);                                    //* 4 points define 1 sphere
    Sphere(const Vector3d&,const Vector3d&,const Vector3d&);    //* 1 point, its normal & 1 point define 1 sphere

    //**Members
    void distance( Vector3d);                             //* Plot the Distance between a point and the Sphere
    void displayInfo();                                         //* Displays the infos of the Sphere in the console
    void openglu(int=50);                                       //* Draws the Sphere inside an OpenGL window
    double dist(Vector3d);                                      //* Compute the distance between a point and the Sphere
};
#endif
