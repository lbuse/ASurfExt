#include "Cones.h"
#include "R_ext/Lapack.h"

typedef Matrix<double, 8, 8> Matrix8d;
typedef Matrix<double, 6, 6> Matrix6d;
typedef Matrix<double, 12, 12> Matrix12d;




    //**Constructors
Cones::
    Cones(){

    }

    //* 2 points with normal define 2 Cones
Cones::
    Cones(const Vector3d& p1,const Vector3d& n,const Vector3d& p2,const Vector3d& n2, string choice){

    // Test if 2 points are equals
    if (((p1-p2).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
    // Test if the Normal is not 0 0 0
    if (((abs(n(0))<THRESHOLD) && (abs(n(1))<THRESHOLD) &&(abs(n(2))<THRESHOLD))||((abs(n2(0))<THRESHOLD) && (abs(n2(1))<THRESHOLD) && (abs(n2(2))<THRESHOLD))){
        std::cerr<< "Normal is undefined"<< endl;
        return;
    }

    Vector3d n1 = n.normalized();
    Matrix3d Rotation;
    double rho = sqrt(n1(1)*n1(1)+n1(2)*n1(2));
    double t = norm(n1);
    Matrix3d Rotation2;
    Rotation2 << rho/t,     -n1(0)*n1(1)/(t*rho),   -n1(0)*n1(2)/(t*rho),
                 0,         n1(2)/rho,              -n1(1)/rho,
                 n1(0)/t,   n1(1)/t,                n1(2)/t;
    if (fabs(rho)<THRESHOLD) {
        rho = sqrt(n1(0)*n1(0)+n1(1)*n1(1));
        Rotation2 <<    -n1(2)*n1(0)/(t*rho),   -n1(2)*n1(1)/(t*rho),   rho/t,
                        n1(1)/rho,              -n1(0)/rho,             0,
                        n1(0)/t,                n1(1)/t,                n1(2)/t;
    }


    Rotation = Rotation2.transpose();

    Vector3d p_1 = Rotation2*(p1-p1);           // p1(0,0,0)
    Vector3d p_2 = Rotation2*(p2-p1);           // p2(x,y,z)
    Vector3d n_1 = (Rotation2*n1);              // n1(0,0,1)
    Vector3d n_2 = (Rotation2*n2).normalized(); // n2(a,b,c)
    double l_1; //lambda 1
    double l_2; //lambda 2
    Vector3d apex;
    Vector3d axis;
    double cos_angle;
    double sin_angle;
    double r0;

    if ((abs(n_2(0)) < THRESHOLD) && (abs(n_2(1)) < THRESHOLD) && (abs(p_2(0)) < THRESHOLD ) && (abs(p_2(1)) < THRESHOLD )){ // n1 and n2 are proportionnal and p2 belongs to N1

        return;

    }
        l_1=dotProduct(p_2,n_2)/(1+n_2(2));
        l_2=-p_2(2)/(n_2(2)+1);

        Vector3d A1 = l_1 * n_1;
        Vector3d A2 = l_2 * n_2 + p_2;


        if (norm(A2-A1)>THRESHOLD){

            axis = (A2-A1).normalized();
            apex=Vector3d(l_1*(n_2(0)*l_2+p_2(0))/(-l_2*n_2(2)+l_1-p_2(2)),
                          l_1*(n_2(1)*l_2+p_2(1))/(-l_2* n_2(2)+l_1-p_2(2)),
                          0);
            sin_angle = norm(crossProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            cos_angle = fabs(dotProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            r0 = std::atan(sin_angle / cos_angle);

            double l1=min(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            double l2=max(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            this->list.push_back(Cone(p1+Rotation*apex,Rotation*axis,r0,l1,l2));

        }
        else{

            axis = (A1-(p_1+p_2)/2).normalized();
            apex = Vector3d(l_1*p_2(0)/(2*l_1-p_2(2)),
                            l_1*p_2(1)/(2*l_1-p_2(2)),
                            0);
            sin_angle = norm(crossProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            cos_angle =fabs(dotProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            r0 =std::atan(sin_angle / cos_angle);

            double l1=min(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            double l2=max(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            this->list.push_back(Cone(p1+Rotation*apex,Rotation*axis,r0,l1,l2));


        }

        l_1=dotProduct(p_2,n_2)/(n_2(2)-1);
        l_2=p_2(2)/(1-n_2(2));

        A1 = l_1 * n_1;
        A2 = l_2 * n_2 + p_2;

        if (norm(A2-A1)>THRESHOLD){
            Vector3d apex;
            Vector3d axis;
            double cos_angle;
            double sin_angle;
            double r0;
            axis = (A2-A1).normalized();
            apex=Vector3d(l_1*(n_2(0)*l_2+p_2(0))/(-l_2*n_2(2)+l_1-p_2(2)),
                          l_1*(n_2(1)*l_2+p_2(1))/(-l_2* n_2(2)+l_1-p_2(2)),
                          0);

            sin_angle = norm(crossProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            cos_angle = fabs(dotProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            r0 = std::atan(sin_angle / cos_angle);

            double l1=min(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            double l2=max(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            this->list.push_back(Cone(p1+Rotation*apex,Rotation*axis,r0,l1,l2));

        }
        else{
            axis = (A1-(p_1+p_2)/2).normalized();
            apex = Vector3d(l_1*p_2(0)/(2*l_1-p_2(2)),
                            l_1*p_2(1)/(2*l_1-p_2(2)),
                            0);
            sin_angle = norm(crossProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            cos_angle =fabs(dotProduct(p_1-apex,axis))/(
                        norm(p_1-apex)*
                        norm(axis));

            r0 = std::atan(sin_angle / cos_angle);

            double l1=min(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            double l2=max(dotProduct(p_1-apex,axis),dotProduct(p_2-apex,axis));
            this->list.push_back(Cone(p1+Rotation*apex,Rotation*axis,r0,l1,l2));

        }

        configuration(choice);



}





    //* 1 point with normal and 3 other points define 0, 2 or 4 Cones
Cones::
    Cones(const Vector3d& p1,const Vector3d& n,const Vector3d& p2,const Vector3d& p3, const Vector3d& p4,string choice){

    // Test if 2 points are equals
    if (((p1-p2).norm() < THRESHOLD) || ((p1-p3).norm() < THRESHOLD) || ((p1-p4).norm() < THRESHOLD)|| ((p2-p3).norm() < THRESHOLD)|| ((p2-p4).norm() < THRESHOLD)|| ((p3-p4).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
    // Test if the Normal is not 0 0 0
    if ((abs(n(0))<THRESHOLD) & (abs(n(1))<THRESHOLD) & (abs(n(2))<THRESHOLD)){
        std::cerr<< "Normal is undefined"<< endl;
        return;
    }


    Vector3d n1 = n.normalized();
    Matrix3d Rotation;
    double rho = sqrt(n1(1)*n1(1)+n1(2)*n1(2));
    double t = norm(n1);
    Matrix3d Rotation2;
    Rotation2 << rho/t,     -n1(0)*n1(1)/(t*rho),   -n1(0)*n1(2)/(t*rho),
                 0,         n1(2)/rho,              -n1(1)/rho,
                 n1(0)/t,   n1(1)/t,                n1(2)/t;
    if (fabs(rho)<THRESHOLD) {
        rho = sqrt(n1(0)*n1(0)+n1(1)*n1(1));
        Rotation2 <<    -n1(2)*n1(0)/(t*rho),   -n1(2)*n1(1)/(t*rho),   rho/t,
                        n1(1)/rho,              -n1(0)/rho,             0,
                        n1(0)/t,                n1(1)/t,                n1(2)/t;
    }
    Rotation = Rotation2.transpose();
    Vector3d p_1 = Rotation2*(p1-p1);   // p1(0,0,0)
    Vector3d p_2 = Rotation2*(p2-p1);   // p2(x,y,z)
    Vector3d p_3 = Rotation2*(p3-p1);   // p3(x,y,z)
    Vector3d p_4 = Rotation2*(p4-p1);   // p4(x,y,z)

    // Permutation of p_2, p_3 and p_4 to get p_2(2) != 0
    if (fabs(p_2(2))<=THRESHOLD){
        if (fabs(p_3(2))<=THRESHOLD){
            if (fabs(p_4(2))<=THRESHOLD){
                return;
            }
            else{
                Vector3d p_i=p_2;
                p_2=p_4;
                p_4=p_i;
            }
        }
        else{
            Vector3d p_i=p_2;
            p_2=p_3;
            p_3=p_i;
        }
    }

    int N = 6;

    // Input Matrix (Eigen)
    Matrix6d A_eigen;
    A_eigen<<   0,  0,  0,  0,  -p_4(2)*power(p_2(2),3)*power(p_3(1),2)-power(p_2(2),2)*power(p_3(2),2)*power(p_4(1),2)+p_3(2)*power(p_2(2),3)*power(p_4(1),2)+power(p_2(2),2)*power(p_4(2),2)*power(p_3(1),2)-p_2(2)*power(p_4(2),2)*p_3(2)*power(p_2(1),2)+p_2(2)*power(p_3(2),2)*p_4(2)*power(p_2(1),2),
                                        -2*power(p_2(2),2)*power(p_4(2),2)*p_3(2)*p_2(0)+2*power(p_2(2),2)*power(p_4(2),2)*p_3(0)*p_3(2)-2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(2)+2*power(p_2(2),2)*power(p_3(2),2)*p_4(2)*p_2(0)-2*power(p_2(2),2)*power(p_3(2),2)*p_4(0)*p_4(2)+2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(2),
                    0,  0,  0,  0,  2*p_2(2)*p_4(0)*p_4(2)*p_3(2)*power(p_2(1),2)-2*p_2(2)*p_3(0)*p_3(2)*p_4(2)*power(p_2(1),2)+2*power(p_2(2),2)*power(p_3(2),2)*p_4(2)*p_2(0)+2*p_4(2)*p_2(0)*power(p_2(2),2)*power(p_3(1),2)-2*power(p_2(2),2)*power(p_3(2),2)*p_4(0)*p_4(2)-2*power(p_2(2),2)*p_4(0)*p_4(2)*power(p_3(1),2)+2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(2)-2*power(p_2(2),2)*power(p_4(2),2)*p_3(2)*p_2(0)-2*p_3(2)*p_2(0)*power(p_2(2),2)*power(p_4(1),2)+2*power(p_2(2),2)*power(p_4(2),2)*p_3(0)*p_3(2)+2*power(p_2(2),2)*p_3(0)*p_3(2)*power(p_4(1),2)-2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(2),
                                        -p_4(2)*power(p_2(2),3)*power(p_3(1),2)-power(p_2(2),2)*power(p_3(2),2)*power(p_4(1),2)+p_3(2)*power(p_2(2),3)*power(p_4(1),2)+power(p_2(2),2)*power(p_4(2),2)*power(p_3(1),2)-p_2(2)*power(p_4(2),2)*p_3(2)*power(p_2(1),2)+p_2(2)*power(p_3(2),2)*p_4(2)*power(p_2(1),2),
                    1,  0,  0,  0,  2*p_2(2)*p_4(1)*p_4(2)*p_3(2)*power(p_2(1),2)-2*p_2(2)*p_3(1)*p_3(2)*p_4(2)*power(p_2(1),2)-2*power(p_2(2),3)*p_3(1)*p_3(2)*p_4(2)+2*p_4(2)*p_2(1)*power(p_2(2),2)*power(p_3(2),2)+2*p_4(2)*p_2(1)*power(p_2(2),2)*power(p_3(1),2)-2*power(p_2(2),2)*p_4(1)*p_4(2)*power(p_3(2),2)-2*power(p_2(2),2)*p_4(1)*p_4(2)*power(p_3(1),2)+2*power(p_2(2),3)*p_4(1)*p_4(2)*p_3(2)-2*p_3(2)*p_2(1)*power(p_2(2),2)*power(p_4(2),2)-2*p_3(2)*p_2(1)*power(p_2(2),2)*power(p_4(1),2)+2*power(p_2(2),2)*p_3(1)*p_3(2)*power(p_4(2),2)+2*power(p_2(2),2)*p_3(1)*p_3(2)*power(p_4(1),2),
                                        4*p_4(2)*p_2(1)*power(p_2(2),2)*p_3(0)*p_3(2)+4*power(p_2(2),2)*p_4(1)*p_4(2)*p_3(2)*p_2(0)-4*power(p_2(2),2)*p_4(1)*p_4(2)*p_3(0)*p_3(2)-4*p_3(2)*p_2(1)*power(p_2(2),2)*p_4(0)*p_4(2)-4*power(p_2(2),2)*p_3(1)*p_3(2)*p_4(2)*p_2(0)+4*power(p_2(2),2)*p_3(1)*p_3(2)*p_4(0)*p_4(2)-2*p_2(2)*power(p_3(2),2)*p_4(2)*p_2(0)*p_2(1)+2*p_2(2)*power(p_4(2),2)*p_3(2)*p_2(0)*p_2(1)+2*power(p_3(2),2)*power(p_2(2),2)*p_4(0)*p_4(1)-2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(1)-2*power(p_4(2),2)*power(p_2(2),2)*p_3(0)*p_3(1)+2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(1),
                    0,  1,  0,  0,  -2*p_2(2)*power(p_3(2),2)*p_4(2)*p_2(0)*p_2(1)-2*p_4(2)*p_2(0)*p_2(1)*p_2(2)*power(p_3(1),2)-2*p_2(2)*p_4(0)*p_4(1)*p_3(2)*power(p_2(1),2)+2*p_2(2)*power(p_4(2),2)*p_3(2)*p_2(0)*p_2(1)+2*p_3(2)*p_2(0)*p_2(1)*p_2(2)*power(p_4(1),2)+2*p_2(2)*p_3(0)*p_3(1)*p_4(2)*power(p_2(1),2)+2*power(p_3(2),2)*power(p_2(2),2)*p_4(0)*p_4(1)+2*power(p_2(2),2)*p_4(0)*p_4(1)*power(p_3(1),2)-2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(1)-2*power(p_4(2),2)*power(p_2(2),2)*p_3(0)*p_3(1)-2*power(p_2(2),2)*p_3(0)*p_3(1)*power(p_4(1),2)+2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(1),
                                        2*p_2(2)*p_4(1)*p_4(2)*p_3(2)*power(p_2(1),2)-2*p_2(2)*p_3(1)*p_3(2)*p_4(2)*power(p_2(1),2)-2*power(p_2(2),3)*p_3(1)*p_3(2)*p_4(2)+2*p_4(2)*p_2(1)*power(p_2(2),2)*power(p_3(2),2)+2*p_4(2)*p_2(1)*power(p_2(2),2)*power(p_3(1),2)-2*power(p_2(2),2)*p_4(1)*p_4(2)*power(p_3(2),2)-2*power(p_2(2),2)*p_4(1)*p_4(2)*power(p_3(1),2)+2*power(p_2(2),3)*p_4(1)*p_4(2)*p_3(2)-2*p_3(2)*p_2(1)*power(p_2(2),2)*power(p_4(2),2)-2*p_3(2)*p_2(1)*power(p_2(2),2)*power(p_4(1),2)+2*power(p_2(2),2)*p_3(1)*p_3(2)*power(p_4(2),2)+2*power(p_2(2),2)*p_3(1)*p_3(2)*power(p_4(1),2),
                    0,  0,  1,  0,  -power(p_2(2),2)*power(p_4(0),2)*power(p_3(1),2)+power(p_2(2),3)*power(p_4(0),2)*p_3(2)+p_4(2)*power(p_2(2),3)*power(p_3(1),2)+power(p_2(2),2)*power(p_3(2),2)*power(p_4(1),2)+power(p_2(2),2)*power(p_3(0),2)*power(p_4(2),2)+power(p_2(2),2)*power(p_3(0),2)*power(p_4(1),2)-power(p_2(2),3)*power(p_3(0),2)*p_4(2)-p_3(2)*power(p_2(2),3)*power(p_4(1),2)-power(p_2(2),2)*power(p_4(2),2)*power(p_3(1),2)-power(p_2(2),2)*power(p_4(0),2)*power(p_3(2),2)+p_2(2)*power(p_4(2),2)*p_3(2)*power(p_2(1),2)+p_2(2)*power(p_4(0),2)*p_3(2)*power(p_2(1),2)+p_4(2)*power(p_2(0),2)*p_2(2)*power(p_3(2),2)+p_4(2)*power(p_2(0),2)*p_2(2)*power(p_3(1),2)-p_2(2)*power(p_3(2),2)*p_4(2)*power(p_2(1),2)-p_2(2)*power(p_3(0),2)*p_4(2)*power(p_2(1),2)-p_3(2)*power(p_2(0),2)*p_2(2)*power(p_4(2),2)-p_3(2)*power(p_2(0),2)*p_2(2)*power(p_4(1),2),
                                        2*p_4(2)*power(p_2(0),2)*p_2(2)*p_3(0)*p_3(2)-4*p_2(1)*p_4(2)*power(p_2(2),2)*p_3(0)*p_3(1)+4*power(p_2(2),2)*p_4(1)*p_4(2)*p_3(0)*p_3(1)-2*p_3(2)*power(p_2(0),2)*p_2(2)*p_4(0)*p_4(2)+4*p_2(1)*p_3(2)*power(p_2(2),2)*p_4(0)*p_4(1)-4*power(p_2(2),2)*p_3(1)*p_3(2)*p_4(0)*p_4(1)+2*power(p_2(2),2)*power(p_4(2),2)*p_3(2)*p_2(0)-2*power(p_2(2),2)*power(p_4(2),2)*p_3(0)*p_3(2)+2*power(p_2(2),2)*power(p_4(0),2)*p_3(2)*p_2(0)-2*power(p_2(2),2)*power(p_4(0),2)*p_3(0)*p_3(2)+2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(2)-2*power(p_2(2),2)*power(p_3(2),2)*p_4(2)*p_2(0)+2*power(p_2(2),2)*power(p_3(2),2)*p_4(0)*p_4(2)-2*power(p_2(2),2)*power(p_3(0),2)*p_4(2)*p_2(0)+2*power(p_2(2),2)*power(p_3(0),2)*p_4(0)*p_4(2)-2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(2)+4*p_2(2)*p_3(1)*p_3(2)*p_4(2)*p_2(0)*p_2(1)-4*p_2(2)*p_4(1)*p_4(2)*p_3(2)*p_2(0)*p_2(1),
                    0,  0,  0,  1,  0,  -power(p_2(2),2)*power(p_4(0),2)*power(p_3(1),2)+power(p_2(2),3)*power(p_4(0),2)*p_3(2)+p_4(2)*power(p_2(2),3)*power(p_3(1),2)+power(p_2(2),2)*power(p_3(2),2)*power(p_4(1),2)+power(p_2(2),2)*power(p_3(0),2)*power(p_4(2),2)+power(p_2(2),2)*power(p_3(0),2)*power(p_4(1),2)-power(p_2(2),3)*power(p_3(0),2)*p_4(2)-p_3(2)*power(p_2(2),3)*power(p_4(1),2)-power(p_2(2),2)*power(p_4(2),2)*power(p_3(1),2)-power(p_2(2),2)*power(p_4(0),2)*power(p_3(2),2)+p_2(2)*power(p_4(2),2)*p_3(2)*power(p_2(1),2)+p_2(2)*power(p_4(0),2)*p_3(2)*power(p_2(1),2)+p_4(2)*power(p_2(0),2)*p_2(2)*power(p_3(2),2)+p_4(2)*power(p_2(0),2)*p_2(2)*power(p_3(1),2)-p_2(2)*power(p_3(2),2)*p_4(2)*power(p_2(1),2)-p_2(2)*power(p_3(0),2)*p_4(2)*power(p_2(1),2)-p_3(2)*power(p_2(0),2)*p_2(2)*power(p_4(2),2)-p_3(2)*power(p_2(0),2)*p_2(2)*power(p_4(1),2);


    Matrix6d B_eigen;
    B_eigen<<   1,  0,  0,  0,  0,  0,
                    0,  1,  0,  0,  0,  0,
                    0,  0,  1,  0,  0,  0,
                    0,  0,  0,  1,  0,  0,
                    0,  0,  0,  0,  0,  2*p_2(2)*power(p_4(2),2)*p_3(2)*p_2(0)*p_2(1)+2*p_2(2)*power(p_4(0),2)*p_3(2)*p_2(0)*p_2(1)+2*p_4(2)*power(p_2(0),2)*p_2(2)*p_3(0)*p_3(1)-2*p_2(2)*power(p_3(2),2)*p_4(2)*p_2(0)*p_2(1)-2*p_2(2)*power(p_3(0),2)*p_4(2)*p_2(0)*p_2(1)-2*p_3(2)*power(p_2(0),2)*p_2(2)*p_4(0)*p_4(1)-2*power(p_4(2),2)*power(p_2(2),2)*p_3(0)*p_3(1)-2*power(p_2(2),2)*power(p_4(0),2)*p_3(0)*p_3(1)+2*p_4(2)*power(p_2(2),3)*p_3(0)*p_3(1)+2*power(p_3(2),2)*power(p_2(2),2)*p_4(0)*p_4(1)+2*power(p_2(2),2)*power(p_3(0),2)*p_4(0)*p_4(1)-2*p_3(2)*power(p_2(2),3)*p_4(0)*p_4(1),
                    0,  0,  0,  0,  0,  0;



    // Converting Eigen Matrix into a linear array of double
    double A[N*N];
    double B[N*N];
    for (int j=0; j<N; j++){
        for(int i=0; i<N; i++){
            A[i+j*N]=A_eigen(i,j);
            B[i+j*N]=B_eigen(i,j);
        }
    }

    double alphar[N];   // alpha's real part
    double alphai[N];   // alpha's imaginary part
    double beta[N];     // beta : eigenvalues lambda_i = (alphar_i+i*alphai_i)/beta_i
    double vl[N*N];     // left eigenvectors (useless)
    double vr[N*N];     // right eigenvectors
    char compute_l='N';  // compute (V) or not (N) the left eigenvectors
    char compute_r='V';  // compute (V) or not (N) the right eigenvectors
    double work[16*N];  // Working variable (minimum size = 8*n)
    int lwork=16*N;     // work's length
    int info;           // information about the success of the operation

    //  DGGEV computes for a pair of N-by-N real nonsymmetric matrices (A,B)
    //  the generalized eigenvalues, and optionally, the left and/or right
    //  generalized eigenvectors. (lapack)
    dggev_(&compute_l, &compute_r,
           &N, A, &N, B, &N,
           alphar, alphai, beta,
           vl, &N, vr, &N,
           work, &lwork, &info);
    // VR_i associated to lambda_i : vr[(0->N-1)+i*N]
    Vector3d apex;
    Vector3d axis;
    double sin_angle;
    double cos_angle;
    double r0;

    // Generating the cones correspondinng to the real non-infinite eigenvalues
    for (int i=0; i<N; i++){
        if(beta[i]!=0 && alphai[i]==0){
            double a=vr[N-2+i*N]/vr[N-1+i*N];
            double b=alphar[i]/beta[i];
            double r=((a*a+b*b-1)*p_2(2)*p_2(2)+2*(a*p_2(0)+b*p_2(1))*p_2(2)+(b*p_2(0)-a*p_2(1))*(b*p_2(0)-a*p_2(1)))/(2*(a*a+b*b)*p_2(2));
            Vector3d q(0,0,r);
            apex=Vector3d(a*r,b*r,0);
            axis=(q-apex).normalized();
            sin_angle = norm(crossProduct(p_1-apex,axis))/(
                norm(p_1-apex)*
                norm(axis));
            cos_angle =fabs(dotProduct(p_1-apex,axis))/(
                norm(p_1-apex)*
                norm(axis));
            r0 = std::atan(sin_angle / cos_angle);

            double l1=min(dotProduct(p_1-apex,axis),min(dotProduct(p_2-apex,axis),min(dotProduct(p_3-apex,axis),dotProduct(p_4-apex,axis))));
            double l2=max(dotProduct(p_1-apex,axis),max(dotProduct(p_2-apex,axis),max(dotProduct(p_3-apex,axis),dotProduct(p_4-apex,axis))));

            this->list.push_back(Cone(p1+Rotation*apex,Rotation*axis,r0,l1,l2));
        }
    }
    configuration(choice);


}

        //* 6 points define up to 12 Cones
        // NOTE : not working
Cones::
    Cones(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3, const Vector3d& p4, const Vector3d& p5, const Vector3d& p6,string choice){



    Vector3d nx = (p2-p1).normalized();
    Vector3d ny = ((p3-p1)-dotProduct(p3-p1,nx)*nx).normalized();
    Vector3d nz = crossProduct(nx,ny).normalized();
    Matrix3d Rotation;
    Matrix3d Rotation2;
    Rotation << nx(0),ny(0),nz(0),
                nx(1),ny(1),nz(1),
                nx(2),ny(2),nz(2);
    Rotation2 = Rotation.transpose();
    Vector3d p_1 = Rotation2*(p1-p1);
    Vector3d p_2 = Rotation2*(p2-p1);
    Vector3d p_3 = Rotation2*(p3-p1);
    Vector3d p_4 = Rotation2*(p4-p1);
    Vector3d p_5 = Rotation2*(p5-p1);
    Vector3d p_6 = Rotation2*(p6-p1);




    if (abs(p_4(2)) < THRESHOLD){
        Vector3d ptemp = p_5;
        p_5 = p_4;
        p_4 = ptemp;
    }
    if (abs(p_4(2)) < THRESHOLD){
        Vector3d ptemp = p_6;
        p_6 = p_4;
        p_6 = ptemp;
    }
    if ((abs(p_2(0)) < THRESHOLD) | (abs(p_3(1)) < THRESHOLD) | (abs(p_4(2)) < THRESHOLD)) {
        return;
    }


    // Input Matrix size (10 non-fonctionnal, 12 now)
    // Size 10 Matrix kept in commentary (with a mistake somewhere)

    int N = 12;

    Matrix12d A_eigen;



    A_eigen <<
            0,  0,  0,  0,  0,  0,  0,  0,  -p_2(0)*p_3(1)*(-p_3(1)*p_4(1)+power(p_4(1),2)+power(p_4(2),2)),
                                                -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_4(0)*p_3(1)*p_4(2),
                                                    -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)+p_2(0)*power(p_3(1),2)*p_4(1)-p_2(0)*p_3(1)*power(p_4(1),2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  -p_2(0)*p_3(1)*(-p_3(1)*p_4(1)+power(p_4(1),2)+power(p_4(2),2)),
                                                    -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_4(0)*p_3(1)*p_4(2),
                                                        -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)+p_2(0)*power(p_3(1),2)*p_4(1)-p_2(0)*p_3(1)*power(p_4(1),2),
            0,  0,  0,  0,  0,  0,  0,  0,  power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                2*p_4(0)*p_3(1)*p_4(2)*p_5(2)-2*p_5(0)*p_3(1)*p_4(2)*p_5(2),
                                                    -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)+power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                    2*p_4(0)*p_3(1)*p_4(2)*p_5(2)-2*p_5(0)*p_3(1)*p_4(2)*p_5(2),
                                                        -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)+power(p_3(1),2)*p_4(1)*p_5(2)-power(p_3(1),2)*p_5(1)*p_4(2)-p_3(1)*power(p_4(1),2)*p_5(2)+p_3(1)*power(p_5(1),2)*p_4(2),
            1,  0,  0,  0,  0,  0,  0,  0,  -p_4(2)*power(p_3(1),2)*p_2(0),
                                                -2*p_2(0)*p_3(0)*p_3(1)*p_4(1)+2*p_2(0)*p_4(0)*p_3(1)*p_4(1),
                                                    power(p_2(0),2)*p_3(0)*p_4(2)-p_2(0)*power(p_3(0),2)*p_4(2)-p_4(2)*power(p_3(1),2)*p_2(0)+2*p_2(0)*p_3(1)*p_4(1)*p_4(2),
                                                        0,
            0,  1,  0,  0,  0,  0,  0,  0,  0,  -p_4(2)*power(p_3(1),2)*p_2(0),
                                                    -2*p_2(0)*p_3(0)*p_3(1)*p_4(1)+2*p_2(0)*p_4(0)*p_3(1)*p_4(1),
                                                        power(p_2(0),2)*p_3(0)*p_4(2)-p_2(0)*power(p_3(0),2)*p_4(2)-p_4(2)*power(p_3(1),2)*p_2(0)+2*p_2(0)*p_3(1)*p_4(1)*p_4(2),
            0,  0,  1,  0,  0,  0,  0,  0,  0,  -2*p_3(0)*p_3(1)*p_4(1)*p_5(2)+2*p_3(0)*p_3(1)*p_5(1)*p_4(2)+2*p_4(0)*p_3(1)*p_4(1)*p_5(2)-2*p_5(0)*p_3(1)*p_5(1)*p_4(2),
                                                    2*p_3(1)*p_4(1)*p_4(2)*p_5(2)-2*p_3(1)*p_5(1)*p_4(2)*p_5(2),
                                                        0,
            0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  -2*p_3(0)*p_3(1)*p_4(1)*p_5(2)+2*p_3(0)*p_3(1)*p_5(1)*p_4(2)+2*p_4(0)*p_3(1)*p_4(1)*p_5(2)-2*p_5(0)*p_3(1)*p_5(1)*p_4(2),
                                                        2*p_3(1)*p_4(1)*p_4(2)*p_5(2)-2*p_3(1)*p_5(1)*p_4(2)*p_5(2),
            0,  0,  0,  0,  1,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_3(0)*p_3(1)*p_4(2),
                                                    -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)-p_2(0)*p_3(1)*power(p_4(2),2),
                                                        0,
            0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(1)*p_4(2)+2*p_2(0)*p_3(0)*p_3(1)*p_4(2),
                                                        -power(p_2(0),2)*p_3(0)*p_4(1)+power(p_2(0),2)*p_4(0)*p_3(1)+p_2(0)*power(p_3(0),2)*p_4(1)-p_2(0)*power(p_4(0),2)*p_3(1)-p_2(0)*p_3(1)*power(p_4(2),2),
            0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  -p_2(0)*p_3(0)*p_4(1)*p_5(2)+p_2(0)*p_3(0)*p_5(1)*p_4(2)+p_2(0)*p_4(0)*p_3(1)*p_5(2)-p_2(0)*p_5(0)*p_3(1)*p_4(2)+power(p_3(0),2)*p_4(1)*p_5(2)-power(p_3(0),2)*p_5(1)*p_4(2)-power(p_4(0),2)*p_3(1)*p_5(2)+power(p_5(0),2)*p_3(1)*p_4(2)-p_3(1)*power(p_4(2),2)*p_5(2)+p_3(1)*p_4(2)*power(p_5(2),2);

    Matrix12d B_eigen;


    B_eigen <<
            1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(0)*p_4(2)+p_2(0)*power(p_3(0),2)*p_4(2),
                                                        0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  -power(p_2(0),2)*p_3(0)*p_4(2)+p_2(0)*power(p_3(0),2)*p_4(2),
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0;

    // Converting Eigen Matrix into a linear array of double
    double A[N*N];
    double B[N*N];
    for (int j=0; j<N; j++){
        for(int i=0; i<N; i++){
            A[i+j*N]=A_eigen(i,j);
            B[i+j*N]=B_eigen(i,j);
        }
    }

    double alphar[N];   // alpha's real part
    double alphai[N];   // alpha's imaginary part
    double beta[N];     // beta : eigenvalues lambda_i = (alphar_i+i*alphai_i)/beta_i
    double vl[N*N];     // left eigenvectors (useless)
    double vr[N*N];     // right eigenvectors
    char compute_l='N';  // compute (V) or not (N) the left eigenvectors
    char compute_r='V';  // compute (V) or not (N) the right eigenvectors
    double work[16*N];  // Working variable (minimum size = 8*n)
    int lwork=16*N;     // work's length
    int info;           // information about the success of the operation
    //  DGGEV computes for a pair of N-by-N real nonsymmetric matrices (A,B)
    //  the generalized eigenvalues, and optionally, the left and/or right
    //  generalized eigenvectors. (lapack)
    dggev_(&compute_l, &compute_r,
           &N, A, &N, B, &N,
           alphar, alphai, beta,
           vl, &N, vr, &N,
           work, &lwork, &info);
    // VR_i associated a lambda_i : vr[(0->N-1)+i*N]
    Vector3d center_bar;
    Vector3d center;
    Vector3d axis;
    double r0;

    // Generating the cones correspondinng to the real non-infinite eigenvalues
    for (int i=0; i<N; i++){
        if(beta[i]!=0 && alphai[i]==0){
            axis = Vector3d(vr[N-2+i*N]/vr[N-1+i*N],alphar[i]/beta[i],1).normalized();
            Vector3d a = p_1 - dotProduct(p_1,axis)*axis;
            Vector3d b = p_2 - dotProduct(p_2,axis)*axis;
            Vector3d c = p_3 - dotProduct(p_3,axis)*axis;

            // lx : length of side opposed to summit x
            double la = norm(b-c);
            double lb = norm(c-a);
            double lc = norm(a-b);
            // sx : easier to read
            double sa = (-la*la + lb*lb + lc*lc);
            double sb = ( la*la - lb*lb + lc*lc);
            double sc = ( la*la + lb*lb - lc*lc);

            center_bar = Vector3d(la*la*sa,lb*lb*sb,lc*lc*sc);
            center_bar = center_bar / (center_bar(0)+center_bar(1)+center_bar(2));

            center = center_bar(0)*a + center_bar(1)*b + center_bar(2)*c;

            r0 = sqrt((la*la*lb*lb*lc*lc) / (4*power(norm(crossProduct(b-a,c-a)),2))); // angle >3.14 ????

            double l1=min(dotProduct(p_1-center,axis),min(dotProduct(p_2-center,axis),min(dotProduct(p_3-center,axis),min(dotProduct(p_4-center,axis),dotProduct(p_5-center,axis)))));
            double l2=max(dotProduct(p_1-center,axis),max(dotProduct(p_2-center,axis),max(dotProduct(p_3-center,axis),max(dotProduct(p_4-center,axis),dotProduct(p_5-center,axis)))));

            this->list.push_back(Cone(p1+Rotation*center,Rotation*axis,r0,l1,l2));
        }
    }
    configuration(choice);
}


void Cones::configuration(string choice){
        std::vector<Cone> list2;
        int n = list.size();
        Cone tpc;
        for (int i=0;i<n;i++){
            tpc = this->list[i];
            if ((tpc.get_Angle()==0)||(tpc.get_Angle()==M_PI)){}
                    else{
                        if ((tpc.getL_max()*tpc.getL_min() >0)&&(choice =="same")){
                            list2.push_back(tpc);
                        }
                        if ((tpc.getL_max()*tpc.getL_min() <0)&&(choice =="oppo")){
                            list2.push_back(tpc);
                        }
                        if ((choice !="same")&&(choice !="oppo")){
                            list2.push_back(tpc);
                        }



                    }


        }
        list= list2;
}





Cone Cones::
    get(int i){
        return this->list[i];
    }

unsigned int Cones::
    size(){
        return this->list.size();
    }

void Cones::
    displayInfo(){
    std::cout<<"\n\nThere are "<<size()<<" Cones.\nHere are the Cones : \n";
        for (unsigned int i=0; i<size(); i++){
            std::cout<<"-----Cone "<<i+1<<"-----\n";
            this->list[i].displayInfo();
        }
    }

void Cones::
    openglu(int precision){
        for (unsigned int i=0; i<size(); i++){
            if(i==0){glColor3f(1.0f,1.0f,0.0f);}
            if(i==0){glColor3f(1.0f,1.0f,0.0f);}
            else if(i==1){glColor3f(1.0f,0.0f,0.0f);}
            else if(i==2){glColor3f(0.0f,1.0f,1.0f);}
            else if(i==3){glColor3f(0.0f,1.0f,0.0f);}
            else if(i==4){glColor3f(0.0f,0.0f,1.0f);}
            else if(i==5){glColor3f(1.0f,0.0f,1.0f);}
            else if(i==6){glColor3f(0.25f,0.75f,0.0f);}
            else if(i==7){glColor3f(0.75f,0.25f,0.0f);}
            else{glColor3f(0.5f,0.5f,1.0f);}
            this->list[i].openglu(precision);
        }
    }

double Cones::
    dotProduct(Vector3d v1, Vector3d v2){
    return v1(0)*v2(0)+v1(1)*v2(1)+v1(2)*v2(2);
}

Vector3d Cones::
    crossProduct(Vector3d v1, Vector3d v2){
    return Vector3d(v1(1)*v2(2)-v1(2)*v2(1),
                    v1(2)*v2(0)-v1(0)*v2(2),
                    v1(0)*v2(1)-v1(1)*v2(0));
}

double Cones::
    norm(Vector3d v){
    return sqrt(dotProduct(v,v));
}

double Cones::
    power(double d, int p){
    double value = d;
    for(int i=1; i<p; i++){
        value = value * d;
    }
    return value;
}

double Cones::
    dist(Vector3d p,int i){
    return this->list[i].dist(p);
}
