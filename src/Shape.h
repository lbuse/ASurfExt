/* ************************************ *
 *          not functionning            *
 * each shape should inherit from Shape *
 * ************************************ */


#ifndef SHAPE_H
#define SHAPE_H

#include <Eigen/Dense>
//#include <QGLViewer/qglviewer.h>
#include <vector>


#define THRESHOLD 0.0001
#define my_pi 3.1415

using namespace Eigen;

class Shape
{
public:
//virtual double dist();
virtual void openglu(int);
virtual void displayInfo();
};

#endif // SHAPE_H
