#ifndef CONE_H
#define CONE_H

#include <Eigen/Dense>
#include <vector>
#include <Sphere.h>
#include <Shape.h>
#include "gl.h"
#include "glu.h"
#include <iostream>

//#include <algorithm>
using namespace Eigen;
using namespace std;

class Cone// : public virtual Shape
{

private:
    //**Attributes
    Vector3d apex;      //* coordinates of the apex
    Vector3d axis;      //* direction of the axis
    double angle;       //* angle of the cone
    double length_max;  //* maximal distance between the apex ant the circular border of the cone
    double length_min;  //* minimal distance between the apex ant the circular border of the cone

public:
    //**Constructors
    Cone();

    Cone(const Vector3d&, const Vector3d&, double, double=-1, double=1); //* Cone define by apex, axis , angle, min and max distances between apex and the center(0,0,0)

    //**Members
    double getL_max();
    double getL_min();
    double get_Angle();
    void distance( Vector3d);                   //* Distance between a random point and the Cone
    //string displayInfo();
    void displayInfo();                         //* Displays the infos of the Cone in the console
    void openglu(int);                          //* Draws the Cone inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    double power(double,int);                   //* Returns double^int
    double dist(Vector3d);                      //* Absolute distance between the figure and a point in argument

};

#endif
