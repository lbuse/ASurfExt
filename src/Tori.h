#ifndef TORI_H
#define TORI_H

#include <Eigen/Dense>
//#include <QGLViewer/qglviewer.h>
#include <vector>
#include <Torus.h>
#include <Shape.h>

using namespace Eigen;

class Tori// : public Shape
{

private:
    //**Attributes
    std::vector<Torus> list;                 //* List of Tori

public:
    //**Constructors
    Tori();
    Tori(const Vector3d&,const Vector3d&,const Vector3d&,
              const Vector3d&, const Vector3d&,const Vector3d& ,string choice = "no");               //* 2 Point with normal, 1 other point define 0 or 2 Tori


    //**Members
    Torus get(int);                             //* Get the Torus in position i in the list
    unsigned int size();                        //* Get the amount of Tori in the list
    void displayInfo();                         //* Displays the informations about all the Tori in the list
    void openglu(int=50);                       //* Draws all the Tori inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    int sign(double);                           //* Sign of a double
    double power(double,int);                   //* Returns double^int
    double dist(Vector3d,int);                      //* Compute the distance between the Torus and the point in argument
    void configuration(string ,const Vector3d& ,const Vector3d& );

};
#endif
