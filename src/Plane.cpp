#include "Plane.h"
#define THRESHOLD 0.0001

Plane::	
    Plane() {
    }

Plane:: 
    Plane(const Vector3d& v1, const Vector3d& v2, const Vector3d& v3) {
        // We take the 3 points to generate 2 directions
        // We use the first point as the origin

    // Test if 2 points are equals
    if (((v1-v2).norm() < THRESHOLD) || ((v1-v3).norm() < THRESHOLD) || ((v2-v3).norm() < THRESHOLD)){
        std::cerr<< "2 points are equals"<< endl;
        return;
    }
        this->origin = v1;
        this->normal = (v2-v1).cross(v3-v1)/((v2-v1).cross(v3-v1)).norm();
    }

Plane:: 
    Plane(const Vector3d& point, const Vector3d& Normal) {

    // Test if the Normal is not 0 0 0
    if ((abs(Normal(0))<THRESHOLD) & (abs(Normal(1))<THRESHOLD) & (abs(Normal(2))<THRESHOLD)){
        std::cerr<< "Normal is undefined"<< endl;
        return;
    }
        this->origin = point;
        this->normal = Normal/Normal.norm();
    }
double Plane::
    dist( Vector3d point){
    return std::abs( this->normal.dot( point-this->origin ) );
}

void Plane::
    distance( Vector3d point){
        double distance = dist(point);
        if (distance <= THRESHOLD) {
            std::cout<<"The distance is = "<<distance<<"\n"
                <<"This point belongs to inlier !"<< std::endl;
        } else {
            std::cout<<"The distance is = "<<distance<<"\n"
                <<"This point belongs to outlier !"<< std::endl;
        }
    }

void Plane::
    displayInfo(){
        std::cout<<"Unit normal vector of current plane is:\n"
            <<this->normal<< std::endl;
        std::cout<<"Its equation is:\n"
            <<this->normal(0)<<"*x + "<<this->normal(1)<<"*y + "
            <<this->normal(2)<<"*z - "
            <<this->origin.dot(this->normal)<<" = 0"<<std::endl;
    }


void Plane::
    openglu(int precision){
        GLUquadric *quad;
        quad = gluNewQuadric();
        glColor3f(0.0f,1.0f,0.0f);
        gluQuadricDrawStyle(quad,GLU_LINE);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(this->origin(0),this->origin(1),this->origin(2));
        GLfloat angle;
        angle = acos(this->normal(2) / normal.norm()) * 180 / M_PI;
        glRotatef(angle,-this->normal(1),this->normal(0),0);
        gluDisk(quad,0,5,precision,precision);
        glPopMatrix();
        gluDeleteQuadric(quad);
    }







