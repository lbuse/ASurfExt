#include "Ransac.h"
#include "Data.h"
#include "Plane.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Cylinders.h"
#include "Cone.h"
#include "Cones.h"
#include "Torus.h"
#include "Tori.h"
#include "Shape.h"
#include "stdio.h"      /* printf, scanf, puts, NULL */
#include "stdlib.h"     /* srand, rand */
#include "time.h"       /* time */


/* *********** *
 * NOT WORKING *
 * *********** */

Ransac::Ransac(PtCloud data){
//Given:
//    data – a set of observed data points
//    model – a model that can be fitted to data points
int k = 1000; // – the maximum number of iterations allowed in the algorithm
double t = 0.1; // – a threshold value for determining when a data point fits a model
double d = 0; // – the number of close data values required to assert that a model fits well to data
int n = 3; // – the minimum number of data values required to fit the model
                        //cone      2/4/6
                        //cylinder  3/5
                        //plane     1/3
                        //sphere    2/4
                        //torus     3

bool stopcondition = false;

while (!stopcondition){
    int iterations = 0;
    shape_wp bestfit;   // meilleure shape actuelle + ses points
    PtCloud data_wo_bestfit;    // reste de data apres retrait des points de bestfit (ecrasera data apres k iterations)
    double besterr = 1000000000;
    while (iterations < k) {
        int maybeinliers[n];    // tirage de n points au hasard dans data
        for (int i=0; i<n; i++){
            int newmaybeinlier = 0;
            while (newmaybeinlier == 0){
                newmaybeinlier=random(data.getSize());
                for (int j=0; j<i; j++){
                    if (newmaybeinlier==maybeinliers[j]){
                        newmaybeinlier = 0;
                    }
                }
            }
            maybeinliers[i]=newmaybeinlier;
        }

        Cylinders co(data.getPoint(maybeinliers[0]),data.getNormal(maybeinliers[0]),data.getPoint(maybeinliers[1]),data.getPoint(maybeinliers[2])); // calcul des cones correspondants
        double err=0;
        std::cout<<iterations<<"\n";
        for (int num_cone=0; num_cone < co.size(); num_cone++){
            PtCloud cones_in;   // inliers du cone courant
            PtCloud cones_out;  // outliers du cone courant
            PtCloud cones_data=data;    // copie de data pour tester les in et les out
            while (!cones_data.empty()){
                double dist =co.dist(cones_data.back().p,num_cone);
                err+=dist;
                if (dist < t){
                    cones_in.push_back(cones_data.back());
                }
                else{
                    cones_out.push_back(cones_data.back());
                }
                cones_data.pop_back();
            }
            if (err < besterr){ // si le cone est le meilleur, on remplace l'ancien meilleur
                besterr = err;
                //Shape bestshape = co.get(num_cone);
                //bestfit.s=&bestshape;
                bestfit.p=cones_in;
                data_wo_bestfit=cones_out;
            }
        }
        iterations ++;
    }
    data = data_wo_bestfit;
    solution.push_back(bestfit);
    stopcondition = (data.getSize()<10);
}
std::cout<<"Solutions : \n";
for (int i=0; i<solution.size(); i++){
    //s->openglu(50);
    //s->displayInfo();
}
}

    //Cylinders   cy_test(PtCloud.getPoint(maybeinliers[1]),PtCloud.getNormal(maybeinliers[1]),PtCloud.getPoint(maybeinliers[2]),PtCloud.getPoint(maybeinliers[3]));
    //Plane       pl_test(PtCloud.getPoint(maybeinliers[1]),PtCloud.getNormal(maybeinliers[1]));
    //Sphere      sp_test(PtCloud.getPoint(maybeinliers[1]),PtCloud.getNormal(maybeinliers[1]),PtCloud.getPoint(maybeinliers[2]));
//    Tori        co_test(PtCloud.getPoint(maybeinliers[1]),PtCloud.getNormal(maybeinliers[1]),PtCloud.getPoint(maybeinliers[2]),PtCloud.getNormal(maybeinliers[2]),,PtCloud.getPoint(maybeinliers[3]));



int Ransac::random(int i)
{
    return ((int)rand()) % i;
}


