#ifndef CYLINDERS_H
#define CYLINDERS_H

#include <Eigen/Dense>
#include <vector>
#include <Cylinder.h>
#include <Shape.h>
#include "gl.h"
#include "glu.h"
#include <iostream>
using namespace Eigen;
using namespace std;

class Cylinders// : public Shape
{

private:
    //**Attributes
    std::vector<Cylinder> list;                 //* List of Cylinders

public:
    //**Constructors
    Cylinders();
    Cylinders(const Vector3d&,const Vector3d&,const Vector3d&,
              const Vector3d&);                                 //* 1 Point with its normal, 2 other points define 0 or 2 cylinders
    Cylinders(const Vector3d&,const Vector3d&,const Vector3d&,
              const Vector3d&,const Vector3d&);                 //* 5 points define up to 6 cylinders

    //**Members
    Cylinder get(int);                          //* Get the Cylinder in position i in the list
    unsigned int size();                        //* Get the amount of Cylinders in the list
    void displayInfo();                         //* Displays the informations about all the Cylinders in the list
    void openglu(int=50);                          //* Draws all the Cylinders inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    int sign(double);                           //* Sign of a double
    double power(double,int);                   //* Returns double^int
    double dist(Vector3d,int);                  //* Absolute distance between the figure in argument (int) and a point in argument (Vector3d)


};
#endif
