#ifndef CYLINDER_H
#define CYLINDER_H

#include <Eigen/Dense>
#include <vector>
#include <Sphere.h>
#include <Shape.h>
#include "gl.h"
#include "glu.h"
#include <iostream>
using namespace Eigen;
using namespace std;

class Cylinder// : public Shape
{

private:
    //**Attributes
    Vector3d center;    //* coordinates of the center of the cylinder
    Vector3d axis;      //* direction of the axis of the cylinder
    double radius;      //* radius of the cylinder
    double length_max;  //* maximal distance between the center(0,0,0) ant the circular border of the cylinder
    double length_min;  //* minimal distance between the center(0,0,0) ant the circular border of the cylinder

public:
    //**Constructors
    Cylinder();
    Cylinder(const Vector3d&,const Vector3d&,double,double=-1.,double=1.); //* Cylinder define by center, axis and radius, min and max distances between border of cylinder and the center(0,0,0)
    Cylinder(const Vector3d&,const Vector3d&,const Vector3d&);      //* Cylinder using 3 points where the cylinder's axis = normal of the plane points

    //**Members
    void distance( Vector3d);             //* Distance between a random point and the Cylinder
    void displayInfo();                         //* Displays the infos of the Cylinder in the console
    void openglu(int=50);                       //* Draws the Cylinder inside an OpenGL window
    double dotProduct(Vector3d,Vector3d);       //* Euclidian dot product
    Vector3d crossProduct(Vector3d,Vector3d);   //* Euclidian cross product
    double norm(Vector3d);                      //* Euclidian norm
    double dist(Vector3d);                      //* Absolute distance between the figure and a point in argument
};
#endif
