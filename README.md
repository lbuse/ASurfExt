**ASurfExt** is a C++ library for computing, extracting and drawing geometric primitives from 3D data sets. 

More details on the [wiki pages](https://gitlab.inria.fr/lbuse/ASurfExt/wikis/home).


**Authors:** Laurent Busé, Adrien Boudin and Antoine Deharveng.


**Required dependencies:**

1. libQGLViewer: 2.6.4
2. Qt:          5.4.2          
3. Lapack:      
4. gcc:         4.9.2
5. config Qt (.pro file)
6. qmake:       1.07a 